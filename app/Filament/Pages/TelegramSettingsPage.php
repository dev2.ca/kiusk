<?php

namespace App\Filament\Pages;

use App\Settings\TelegramSettings;
use Artisan;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\FileUpload;
use Filament\Pages\SettingsPage;
use Illuminate\Support\Str;
use Filament\Pages\Actions\ButtonAction;
use Telegram;

class TelegramSettingsPage extends SettingsPage
{
 protected static ?string $navigationGroup = 'تنظیمات';
 protected static ?string $navigationIcon = 'heroicon-o-cog';
 protected static ?string $navigationLabel = 'بخش تلگرام';
 protected static ?string $title = 'بخش تلگرام';
 protected static string $settings = TelegramSettings::class;

 protected function getFormSchema(): array
 {
  return [
   Section::make('اتصال ربات')
          ->schema([
                    Placeholder::make('a')
                               ->label('با جستجو Botfather@ در تلگرام میتوانید api را بدست آورید.'),
                    TextInput::make('botApiToken')
                             ->label('Api ریات')
                             ->helperText('بعد از تغییر روی دکمه اتصال ربات کلیک کنید.'),
                   ])
          ->collapsed(),
   Section::make('شروع')
          ->schema([
                    FileUpload::make('startImage')
                              ->label(__('setting.startImage'))
                              ->directory('setting/telegram'),
                    TextInput::make('startText')
                             ->label(__('setting.startText')),
                    Section::make(' کیبورد start')
                           ->schema([
                                     Repeater::make('startKeyboard')
                                             ->label(__('setting.startKeyboard'))
                                             ->schema([
                                                       TextInput::make('keyName')
                                                                ->label(__('setting.keyName'))
                                                                ->required(),
                                                      ])
                                             ->disableItemMovement()
                                             ->disableItemCreation()
                                             ->disableItemDeletion()
                                    ])
                           ->collapsed(),
                   ])
          ->collapsed(),
   Section::make(' ثبت نام')
          ->schema([
                    TextInput::make('registerText')
                             ->label(__('setting.registerText')),
                    TextInput::make('registerSuccess')
                             ->label(__('setting.registerSuccess')),
                    Section::make(' کیبورد ثبت نام')
                           ->schema([
                                     Repeater::make('registerKeyboard')
                                             ->label(__('setting.registerKeyboard'))
                                             ->schema([
                                                       TextInput::make('keyName')
                                                                ->label(__('setting.keyName'))
                                                                ->required(),
                                                      ])
                                             ->disableItemMovement()
                                             ->disableItemCreation()
                                             ->disableItemDeletion()
                                    ])
                           ->collapsed(),
                   ])
          ->collapsed(),
   Section::make('پروفایل')
          ->schema([
                    TextInput::make('profileText')
                             ->label(__('setting.profileText')),
                    Section::make(' کیبورد پروفایل')
                           ->schema([
                                     Repeater::make('profileKeyboard')
                                             ->label(__('setting.profileKeyboard'))
                                             ->schema([
                                                       TextInput::make('keyName')
                                                                ->label(__('setting.keyName'))
                                                                ->required(),
                                                       TextInput::make('keyText')
                                                                ->label(__('setting.keyText'))
                                                                ->required(),
                                                       TextInput::make('keyRule')
                                                                ->label(__('setting.keyRule'))
                                                                ->required(),
                                                      ])
                                             ->helperText('<a href="https://laravel.com/docs/8.x/validation">برای قواعد اعتبارسنجی بیشتر به اینجا مراجعه کنید.</a>')
                                             ->disableItemMovement()
                                             ->disableItemCreation()
                                             ->disableItemDeletion()
                                    ])
                           ->collapsed()
                   ])
          ->collapsed(),
   Section::make('ایجاد آگهی')
          ->schema([
                    TextInput::make('adsCreateText')
                             ->label(__('setting.adsCreateText')),
                    TextInput::make('adsCreateSuccess')
                             ->label(__('setting.adsCreateSuccess')),
                    Section::make(' کیبورد ایجاد آگهی')
                           ->schema([
                                     Repeater::make('adsCreateKeyboard')
                                             ->label(__('setting.adsCreateKeyboard'))
                                             ->schema([
                                                       TextInput::make('keyName')
                                                                ->label(__('setting.keyName'))
                                                                ->required(),
                                                       TextInput::make('keyText')
                                                                ->label(__('setting.keyText'))
                                                                ->required(),
                                                       TextInput::make('keyRule')
                                                                ->label(__('setting.keyRule'))
                                                                ->required(),
                                                      ])
                                             ->disableItemMovement()
                                             ->disableItemCreation()
                                             ->disableItemDeletion()
                                    ])
                           ->collapsed()
                   ])
          ->collapsed(),
   Section::make('ویرایش آگهی')
          ->schema([
                    TextInput::make('adsEditText')
                             ->label(__('setting.adsEditText')),
                    TextInput::make('adsEditSuccess')
                             ->label(__('setting.adsEditSuccess')),
                    Section::make(' کیبورد ویرایش آگهی')
                           ->schema([
                                     Repeater::make('adsEditKeyboard')
                                             ->label(__('setting.adsEditKeyboard'))
                                             ->schema([
                                                       TextInput::make('keyName')
                                                                ->label(__('setting.keyName'))
                                                                ->required(),
                                                       TextInput::make('keyText')
                                                                ->label(__('setting.keyText'))
                                                                ->required(),
                                                       TextInput::make('keyRule')
                                                                ->label(__('setting.keyRule'))
                                                                ->required(),
                                                      ])
                                             ->disableItemMovement()
                                             ->disableItemCreation()
                                             ->disableItemDeletion()
                                    ])
                           ->collapsed()
                   ])
          ->collapsed(),
   Section::make('آگهی‌های من')
          ->schema([
                    TextInput::make('adsListText')
                             ->label(__('setting.adsListText')),
                    TextInput::make('adsListTextEmpty')
                             ->label(__('setting.adsListTextEmpty')),
                    TextInput::make('adsListIsNotVisible')
                             ->label(__('setting.adsListIsNotVisible')),
                    TextInput::make('adsListBack')
                             ->label(__('setting.adsListBack')),
                   ])
          ->collapsed(),
   Section::make('قبول قوانین')
          ->schema([
                    TextInput::make('adsAcceptTheRulesKeyName')
                             ->label(__('setting.adsAcceptTheRulesKeyName')),
                    Textarea::make('adsAcceptTheRulesText')
                            ->label(__('setting.adsAcceptTheRulesText'))
                            ->helperText("
\*\*bold\*\*
 <br>
**bold**
<br>
\*italic\*
<br>
*italic*
<br>
\`code\`
<br>
`code`
<br>
\`\`\`c++
<br>
code
<br>
\`\`\`
<br>
```c++
code
```
<br>
A [link](http://example.com).

A [link](http://example.com).
"),
                   ])
          ->collapsed(),
  ];
 }

 public static function getSlug(): string
 {
  return static::$slug ?? Str::kebab(class_basename(static::class));
 }

 protected function getBreadcrumbs(): array
 {
  return [
   '/admin/client-side-setting' => 'تنظیمات',
   'بخش تلگرام'
  ];
 }

 protected function getActions(): array
 {
  return [
   ButtonAction::make('setwebhook')
               ->label('اتصال ربات')
               ->action('setwebhook'),
   ButtonAction::make('refresh')
               ->label('ریست تنظیمات')
               ->color('danger')
               ->icon('heroicon-s-exclamation-circle')
               ->action('refresh')
  ];
 }

 public function setwebhook(): void
 {
  Telegram::setWebhook(['url' => env('APP_URL') . st()->botApiToken]);
  $this->notify('success', 'اتصال برقرار شد.');
 }

 public function refresh()
 {
  Artisan::call("migrate:refresh --path=database/settings/2022_02_24_185717_telegram_settings_migration_reset.php");
  $this->redirect(Self::getSlug());
  $this->notify('success', 'تنظیمات ریست شد.');
 }
}

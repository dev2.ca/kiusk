<?php

namespace App\Filament\Resources\Ad\AdResource\Pages;

use App\Filament\Resources\Ad\AdResource;
use App\Models\Ad\Ad;
use Filament\Resources\Pages\CreateRecord;

class CreateAd extends CreateRecord
{
 protected static string $resource = AdResource::class;
}

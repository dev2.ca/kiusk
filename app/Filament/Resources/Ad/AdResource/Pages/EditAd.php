<?php

namespace App\Filament\Resources\Ad\AdResource\Pages;

use App\Filament\Resources\Ad\AdResource;
use App\Models\Ad\Ad;
use App\Rules\Seo;
use Filament\Pages\Actions\ButtonAction;
use Filament\Resources\Pages\EditRecord;

class EditAd extends EditRecord
{
 protected static string $resource = AdResource::class;

 protected function getActions(): array
 {
  return array_merge(parent::getActions(), [
   ButtonAction::make(__('admin.toTop'))
               ->action('toTop'),
   ButtonAction::make(__('admin.show'))
               ->openUrlInNewTab()
               ->url(route('front.ad.show', $this->record)),
  ]);
 }

 public function toTop(): void
 {
  /**
   * @var Ad $ad
   * */
  $ad = $this->record;
  $ad->moveToEnd();
  $this->notify('success', __('admin.toTopDone'));
 }

 protected function beforeValidate(): void
 {
//  $this->seoEditPageBeforeValidate();
  $record = $this->record;
  /**
   * @var Ad $record
   * */
  $record->update(['created_at' => $this->data['created_at']]);
 }

 public function updatedData($value, $key)
 {
  $this->seoEditPageBeforeValidate();
 }

 public function seoEditPageBeforeValidate(): void
 {
  $this->validate([
                   'data.seo_description' => new Seo($this->data['title'], 'des'),
                   'data.seo_title' => new Seo($this->data['title'])
                  ]);
 }
}

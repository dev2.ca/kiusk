<?php

namespace App\Filament\Resources\Ad;

use App\Filament\Resources\Ad\CategoryResource\Pages;
use App\Filament\Resources\Ad\CategoryResource\RelationManagers;
use App\Filament\Resources\Lib\Seo;
use App\Models\Ad\Category;
use Filament\Forms;
use Filament\Forms\Components\BelongsToSelect;
use Filament\Forms\Components\Card;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\MarkdownEditor;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\RichEditor;
use Filament\Forms\Components\SpatieMediaLibraryFileUpload;
use Filament\Forms\Components\SpatieTagsInput;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\BooleanColumn;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Spatie\Tags\Tag;

class CategoryResource extends Resource
{
 use Seo;

 protected static ?string $navigationLabel = 'دسته‌بندی';
 protected static ?string $label = 'دسته‌بندی ';
 protected static ?string $pluralLabel = 'دسته‌بندی ها';
 protected static ?string $model = Category::class;
 protected static ?string $navigationIcon = 'heroicon-o-collection';
 protected static ?string $navigationGroup = 'بخش آگهی ';

 public static function form(Form $form): Form
 {
  $schema = [
   Card::make()
       ->schema([
                 Grid::make()
                     ->schema([
                               TextInput::make('name')
                                        ->label(__('admin.name'))
                                        ->required()
                                        ->reactive()
                                        ->afterStateUpdated(fn($state, callable $set) => $set('slug',
                                                                                              Str::slug($state))),
                               TextInput::make('slug')
                                        ->label(__('admin.slug'))
                                        ->disabled()
                                        ->required()
                                        ->unique(Category::class, 'slug', fn($record) => $record),
                               Toggle::make('is_visible')
                                     ->label(__('admin.is_visible'))
                                     ->default(true),
                               TextInput::make('position')
                                        ->label(__('admin.position'))
                                        ->default(0)
                                        ->required()
                                        ->numeric(),
                              ]),
                 BelongsToSelect::make('parent_id')
                                ->label(__('admin.parent_id'))
                                ->relationship('parent', 'name')
                                ->searchable()
                                ->placeholder('Select parent category'),
                 RichEditor::make('description')
                           ->label(__('admin.description'))
                           ->disableToolbarButtons([
                                                    'attachFiles',
                                                    'codeBlock',
                                                   ]),
                 SpatieTagsInput::make('tags')
                                ->label(__('admin.tags'))
                                ->type('ad')
                                ->suggestions(function () {
                                 $vars = Tag::whereIn('type', [
                                  'ad',
                                  'adCategory'
                                 ])
                                            ->get('name')
                                            ->toArray();
                                 return Arr::flatten($vars);
                                }),
                 SpatieMediaLibraryFileUpload::make('SpecialImage')
                                             ->label(__('admin.SpecialImage'))
                                             ->collection('SpecialImage')
                ])
       ->columnSpan(2),
   Card::make()
       ->schema([
                 Placeholder::make('created_at')
                            ->label(__('admin.created_at'))
                            ->content(fn(?Category $record): string => $record ? $record->created_at->diffForHumans() : '-'),
                 Placeholder::make('updated_at')
                            ->label(__('admin.updated_at'))
                            ->content(fn(?Category $record): string => $record ? $record->updated_at->diffForHumans() : '-'),
                ])
       ->columnSpan(1),
   self::seoInputs('name')
  ];
  return $form->schema($schema)
              ->columns(3);
 }

 public static function table(Table $table): Table
 {
  $columns = [
   TextColumn::make('name')
             ->label(__('admin.name'))
             ->searchable()
             ->sortable(),
   TextColumn::make('parent.name')
             ->label(__('admin.parent.name'))
             ->searchable()
             ->sortable(),
   BooleanColumn::make('is_visible')
                ->label(__('admin.is_visible'))
                ->sortable(),
   TextColumn::make('updated_at')
             ->formatStateUsing(function ($state) {
              return jdate($state)->format('j F Y');
             })
             ->label(__('admin.updated_at'))
             ->sortable(),
  ];
  return $table->columns($columns)
               ->filters([//
                         ]);
 }

 public static function getRelations(): array
 {
  return [
   RelationManagers\AttributesRelationManager::class
  ];
 }

 public static function getPages(): array
 {
  return [
   'index' => Pages\ListCategories::route('/'),
   'create' => Pages\CreateCategory::route('/create'),
   'edit' => Pages\EditCategory::route('/{record}/edit'),
  ];
 }
}

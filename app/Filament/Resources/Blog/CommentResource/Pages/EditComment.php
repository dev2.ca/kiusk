<?php

namespace App\Filament\Resources\Blog\CommentResource\Pages;

use App\Filament\Resources\Blog\CommentResource;
use Filament\Resources\Pages\EditRecord;

class EditComment extends EditRecord
{
 protected static string $resource = CommentResource::class;
}

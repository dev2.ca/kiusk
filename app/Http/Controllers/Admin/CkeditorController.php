<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Ad\Ad;

class CkeditorController extends Controller
{
 public function upload()
 {
  $str = request()->model_type;
//  $str = "App\\Models\\Ad\\Ad";
  $model = (new $str())->find(request()->model_id);
  /**
   * @var Ad $model
   * */
  $url = $model->addMediaFromRequest('upload')
               ->toMediaCollection('content')
               ->getUrl();
  return response()->json([
                           'url' => $url,
                          ]);
 }
}

<?php

namespace App\Http\Controllers\Front\Panel\User;

use App\Http\Controllers\Controller;
use App\Models\Ad\Ad;

class UserPanelController extends Controller
{
 public function frontPanelUserAdIndex()
 {
  return view('front.pages.panel.user.ads.index');
 }

 public function frontPanelUserAdEdit($id)
 {
//return
  $ad = Ad::whereId($id)
          ->whereUserId(auth()->id())
          ->firstOrFail();
  return view('front.pages.panel.user.ads.edit', compact('ad'));
 }

 public function frontPanelUserAdPayment($id)
 {
  $ad = Ad::whereId($id)
          ->whereUserId(auth()->id())
          ->firstOrFail();
  return view('front.pages.panel.user.ads.payment', compact('ad'));
 }

 public function frontPanelUserFavoriteIndex()
 {
  return view('front.pages.panel.user.favorites.index');
 }

 public function frontPanelUserPaymentIndex()
 {
  return view('front.pages.panel.user.payments.index');
 }

 public function frontPanelUserProfileEdit()
 {
  return view('front.pages.panel.user.profile.edit');
 }

 public function frontPanelUserProfileShow()
 {
  return view('front.pages.panel.user.profile.show');
 }
}

<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Models\Ad\Ad;
use App\Models\Payment\Discount;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Illuminate\Http\Request;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;

/** All Paypal Details class **/

use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use Redirect;
use Session;
use URL;

class PaymentController extends Controller
{
 private ApiContext $_api_context;

 public function __construct()
 {
  /** PayPal api context **/
  $paypal_conf = \Config::get('paypal');
  $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
  $this->_api_context->setConfig($paypal_conf['settings']);
 }

 public function index()
 {
  return view('paywithpaypal');
 }

 public function payWithpaypal($name = 'Item 1', $price = 0, $description = '', $returnUrl, $cancelUrl)
 {
  $payer = new Payer();
  $payer->setPaymentMethod('paypal');
  $item_1 = new Item();
  $item_1->setName($name)/** item name **/
         ->setCurrency('USD')
         ->setQuantity(1)
         ->setPrice($price);
  /** unit price **/
  $item_list = new ItemList();
  $item_list->setItems([$item_1]);
  $amount = new Amount();
  $amount->setCurrency('USD')
         ->setTotal($price);
  $transaction = new Transaction();
  $transaction->setAmount($amount)
              ->setItemList($item_list)
              ->setDescription($description);
  $redirect_urls = new RedirectUrls();
  $redirect_urls->setReturnUrl(route('payment'))/** Specify return URL **/ ->setCancelUrl(route('payment'));
  $payment = new Payment();
  $payment->setIntent('Sale')
          ->setPayer($payer)
          ->setRedirectUrls($redirect_urls)
          ->setTransactions([$transaction]);
//  dd($payment->create($this->_api_context));exit;
  try {
   $payment->create($this->_api_context);
  }
  catch (\PayPal\Exception\PayPalConnectionException $ex) {
   if (\Config::get('app.debug')) {
    \Session::put('error', 'Connection timeout');
    return Redirect::to($cancelUrl);
   }
   else {
    \Session::put('error', 'Some error occur, sorry for inconvenient');
    return Redirect::to($cancelUrl);
   }
  }
  foreach ($payment->getLinks() as $link) {
   if ($link->getRel() == 'approval_url') {
    $redirect_url = $link->getHref();
    break;
   }
  }
  /** add payment ID to session **/
  Session::put('paypal_payment_id', $payment->getId());
  if (isset($redirect_url)) {
   /** redirect to paypal **/
   return Redirect::away($redirect_url);
  }
  \Session::put('error', 'Unknown error occurred');
  return Redirect::to($cancelUrl);
 }

 public function getPaymentStatus()
 {
  /** Get the payment ID before session clear **/
  $payment_id = Session::get('paypal_payment_id');
  /** clear the session payment ID **/
  Session::forget('paypal_payment_id');
  $route = Session::get('paymentReturnBackRouteUrl');
  Session::forget('paymentReturnBackRouteUrl');
  if (empty(request()->PayerID) || empty(\request()->token)) {
   \Session::put('error', 'Payment failed');
   return Redirect::to($route);
  }
  $payment = Payment::get($payment_id, $this->_api_context);
  $execution = new PaymentExecution();
  $execution->setPayerId(\request()->PayerID);
  /**Execute the payment **/
  $result = $payment->execute($execution, $this->_api_context);
  if ($result->getState() == 'approved') {
//   // dump(\request()->paymentReturnBackRouteName);
   switch (Session::get('paymentReturnBackRouteName')) {
    case 'front.ad.create':
     $adSession = \Session::get('paymentObject');
//     // dump($adSession);
     $ad = new Ad($adSession);
     $ad->id = $adSession['id'];
     $discountSession = (array)Session::get('paymentDiscount');
     $discount = new Discount($discountSession);
     $total = Session::get('paymentTotalAmount');
     Session::forget('paymentTotalAmount');
     $price = Session::get('paymentPrice');
     Session::forget('paymentPrice');
     $extra = [
      'payment_info' => [
       'gateway' => 'paypal',
       'payer_id' => \request()->PayerID,
       'payment_id' => $payment_id,
       'token' => \request()->token
      ],
      'payment_price' => [
       'total' => $total,
       'price' => $price,
       'discount_id' => $discount->id
      ],
      'ad' => [
       'special' => true,
       'name' => 'آگهی ویژه'
      ]
     ];
     \DB::transaction(function () use ($ad, $discount, $total, $extra) {
      $adExtra = $ad->extra;
      $adExtra->special = true;
      Ad::find($ad->id)
        ->//      $ad->
        update([
                'extra' => $adExtra,
               ]);
      $payment = $ad->payments()
                    ->create([
                              'amount' => $total,
                              'user_id' => auth()->id(),
                              'status' => 'complete',
                              'extra' => $extra
                             ]);
      Discount::whereCode($discount->code)
              ->update([
                        'payment_id' => $payment->id,
                       ]);
     });
     break;
    case 'front.panel.user.ad.payment':
     $adSession = \Session::get('paymentObject');
//     // dump($adSession);
     $ad = new Ad($adSession);
     $ad->id = $adSession['id'];
     $discountSession = (array)Session::get('paymentDiscount');
     $discount = new Discount($discountSession);
     $total = Session::get('paymentTotalAmount');
     Session::forget('paymentTotalAmount');
     $price = Session::get('paymentPrice');
     Session::forget('paymentPrice');
     $name = Session::get('paymentName');
     Session::forget('paymentName');
     if ($name !== 'نردبان') {
      $special = true;
     }
     else {
      $special = false;
     }
     $extra = [
      'payment_info' => [
       'gateway' => 'paypal',
       'payer_id' => \request()->PayerID,
       'payment_id' => $payment_id,
       'token' => \request()->token
      ],
      'payment_price' => [
       'total' => $total,
       'price' => $price,
       'discount_id' => $discount->id
      ],
      'ad' => [
       'special' => $special,
       'name' => $name
      ]
     ];
     \DB::transaction(function () use ($ad, $discount, $total, $extra, $special, $name) {
      $adExtra = $ad->extra;
      $adExtra->special = $special;
      $ad1 = Ad::find($ad->id);
      $ad1->update([
                    'extra' => $adExtra,
                   ]);
      if (\Str::contains($name, 'نردبان')) {
       $ad1->moveToEnd();
      }
      $payment = $ad->payments()
                    ->create([
                              'amount' => $total,
                              'user_id' => auth()->id(),
                              'status' => 'complete',
                              'extra' => $extra
                             ]);
      Discount::whereCode($discount->code)
              ->update([
                        'payment_id' => $payment->id,
                       ]);
     });
     break;
   }
   \Session::put('success', 'پرداخت با موفقیت انجام شد.کد پرداخت : ' . $payment_id);
   Session::get('successPayment', true);
   return Redirect::to($route);
  }
  \Session::put('error', 'پرداخت ناموفق');
  return Redirect::to($route);
 }
}
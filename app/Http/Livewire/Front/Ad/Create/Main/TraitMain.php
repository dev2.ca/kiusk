<?php

namespace App\Http\Livewire\Front\Ad\Create\Main;

use App\Models\Ad\Ad;

trait TraitMain
{
 public Ad $ad;
 public $content;
 public string $step = 'category';

 public function goTo($to)
 {
  if ($to === 'category') {
//   $this->resetExcept('categories');
   $this->step = 'category';
//   $this->photos = [];
   $this->ad = new Ad();
  }
  if ($to === 'form') {
   $this->formAttributes = \App\Models\Ad\Category::
   find((int)$this->selectedCategory)?->attrs->toArray();
  }
  if ($to === 'review') {
   $this->validationAll();
  }
  $this->step = $to;
 }

 public array $formAttributes = [];

 public function updated($V, $n)
 {
//  dump($V, $n);
 }

 public function updatedFormAttributes($v)
 {
 }

 public bool $showEmail = false;
 public function store()
 {
  $this->validate();
  $ad = $this->ad;
  $ad->slug = \Str::slug($ad->title);
  $ad->is_visible = false;
  $ad->content = strip_tags($this->content);
  $ad->user_id = auth()->id();
//  $stdClass = new \stdClass();
//  $stdClass->showEmail = $this->showEmail;
//  $ad->extra = $stdClass;
  $ad->is_visible_email = !$this->showEmail;
  $ad->save();
//  $this->storedAd = $ad;
  \Session::put('paymentObject', $ad->toArray());
  $medias = auth()
   ->user()
   ->media()
   ->whereCollectionName('newAdGalleryWeb')
   ->get();
  $ad->categories()
     ->attach($this->selectedCategory, ['is_main' => true]);
  foreach ($medias as $key => $media) {
   if ($key === 0) {
    $media->move($ad, 'SpecialImage');
   }
   else {
    $media->move($ad, 'Gallery');
   }
  }
  foreach ($this->formAttributes as $attribute) {
//   $ad->attrs()->attach([
//    $attribute['id']=>['text'=>$attribute['text']]
//                        ]);
   $ad->attrs()
      ->attach($attribute['id'], ['text' => $attribute['text']]);
  }
//  $this->reset();
//  $this->step = 'category';
//  $this->backToCategory = 0;
//  $this->selectedCategory = '';
  $this->categories = [...$this->getFirstParent()];
  $this->reset('backToCategory', 'selectedCategory');
  $this->photos = [];
  $this->ad = new Ad();
  $this->formAttributes = [];
  $this->goTo('buy');
  $this->dispatchBrowserEvent('swal:modal', [
   'icon' => 'success',
   'title' => 'آگهی با موفقیت ایجاد شد.پس از تایید منتشر خواهد شد.',
   'timerProgressBar' => true,
   'timer' => 20000,
   'confirmButtonText' => '<i class="fa fa-thumbs-up"></i> متوجه شدم',
   'width' => 300
  ]);
 }

 public function validationAll(): void
 {
  $list = [];
  foreach ($this->formAttributes as $key => $attribute) {
   if ($attribute['validation'] !== null) {
    $list['formAttributes.' . $key . '.text'] = $attribute['validation'];
   }
  }
  $listName = [];
  foreach ($this->formAttributes as $key => $attribute) {
   if ($attribute['validation'] !== null) {
    $listName['formAttributes.' . $key . '.text'] = $attribute['name'];
   }
  }
  $list = array_merge($this->rules, $list);
  $listName = array_merge($this->validationAttributes, $listName);
  $this->validate($list, [], $listName);
 }
}

<?php

namespace App\Http\Livewire\Front\Ad;

use App\Models\Ad\Ad;
use App\Models\User;
use Cookie;
use DB;
use Livewire\Component;

class Show extends Component
{
 use Favorite;

 public Ad $ad;
 public string $email = '';
 public string $name = '';
 public string $comment = '';
 protected $rules = [
  'email' => 'required|email',
  'name' => 'required|min:3',
  'comment' => 'required',
 ];
 protected $validationAttributes = [
  'email' => 'ایمیل',
  'name' => 'نام',
  'comment' => 'دیدگاه',
 ];
 protected $listeners = [
  'reportConfirm',
  'viewed'
 ];
 public $currentUrl;

 public function mount()
 {
  $this->mountFavorite();
 }

 public function storeComment()
 {
  $user = User::firstOrCreate(['email' => $this->email,], [
   'name' => $this->name,
   'rule' => 'subscriber'
  ]);
  $this->ad->reviews()
           ->create([
                     'content' => $this->comment,
                     'user_id' => $user->id,
                    ]);
  $this->dispatchBrowserEvent('swal:modal', [
   'icon' => 'success',
   'title' => 'دیدگاه شما با موفقیت ثبت شد.',
   'timerProgressBar' => true,
   'timer' => 20000,
   'confirmButtonText' => '<i class="fa fa-thumbs-up"></i> متوجه شدم',
   'width' => 300
  ]);
  $this->reset('email', 'name', 'comment');
 }

 public function showContactInfo()
 {
  $phone = $this->ad->phone !== '' && $this->ad->phone !== null ? $this->ad->phone : $this->ad?->user->phone;
  $email = $this->ad->email !== '' && $this->ad->email !== null ? $this->ad->email : $this->ad?->user->email;
  $html = "شماره تماس:<br>
   
<a href=\"tel:{$phone}\">{$phone}</a>
   ";
  if ($this->ad->is_visible_email) {
//  if ((isset($this->ad->extra->showEmail) && $this->ad->extra->showEmail) || !isset($this->ad->extra->showEmail)) {
   $html .= "   <br>
   ایمیل:
   <br>
      
<a href=\"mailto:{$email}\">{$email}</a>
";
  }
  $this->dispatchBrowserEvent('swal:modal', [
   'title' => 'اطلاعات تماس',
   'timerProgressBar' => true,
   'timer' => 20000,
   'html' => $html,
   'confirmButtonText' => '<i class="fa fa-thumbs-up"></i> متوجه شدم',
   'width' => 300
  ]);
 }

 public function report()
 {
  $this->dispatchBrowserEvent('swal:confirm2', [
   'icon' => 'info',
   'title' => 'دلیل گزارش خود را انتخاب کنید.',
   'timerProgressBar' => true,
   'timer' => 20000,
   'confirmButtonText' => ' ارسال',
   'cancelButtonText' => ' صرف نظر',
   'showCancelButton' => true,
   'width' => 600,
   'input' => 'select',
   'inputOptions' => [
    'محتوای آگهی نامناسب است.' => 'محتوای آگهی نامناسب است.',
    'اطلاعات آگهی گمراه کننده یا دروغ است.' => 'اطلاعات آگهی گمراه کننده یا دروغ است.',
    ' آگهی اسپم است و چندین بار پست شده است.' => ' آگهی اسپم است و چندین بار پست شده است.',
    'آگهی در دسته بندی نامناسب قرار گرفته است.' => 'آگهی در دسته بندی نامناسب قرار گرفته است.',
    ' خدمات کالا یا املاک دیگر موجود نیست.' => ' خدمات کالا یا املاک دیگر موجود نیست.',
    'کالا یا خدمات قرار گرفته مشمول فهرست مصادیق محتوای مجرمانه می باشد.' => 'کالا یا خدمات قرار گرفته مشمول فهرست مصادیق محتوای مجرمانه می باشد.',
    ' دلایل دیگر...' => ' دلایل دیگر...',
   ],
   'event' => 'reportConfirm',
   'customClass' => [
    'input' => 'swal_input',
   ]
  ]);
 }

 public function reportConfirm($a)
 {
  if ($a['isConfirmed']) {
   $this->ad->reports()
            ->create([
                      'title' => $a['value'],
                      'user_id' => auth()->check() ? auth()->id() : null
                     ]);
   $this->dispatchBrowserEvent('swal:modal', [
    'icon' => 'success',
    'title' => 'گزارش شما با موفقیت ثبت شد.',
    'timerProgressBar' => true,
    'timer' => 20000,
    'confirmButtonText' => '<i class="fa fa-thumbs-up"></i> متوجه شدم',
    'width' => 300
   ]);
  }
 }

 public function viewed()
 {
  DB::table($this->ad->getTable())
    ->where('id', $this->ad->id)
    ->update(['views' => $this->ad->views + 1]);
 }

 public function render()
 {
  return view('livewire.front.ad.show');
 }
}

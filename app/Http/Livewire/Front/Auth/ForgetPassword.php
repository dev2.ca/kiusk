<?php

namespace App\Http\Livewire\Front\Auth;

use App\Models\User;
use Hash;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Validation\Rules\Password as RulePassword;
use Livewire\Component;
use Mail;
use Password;
use Str;
use Telegram\Bot\Api;

class ForgetPassword extends Component
{
 protected $listeners = ['toggleFormForgetPassword'];
 public bool $showForm = false;
 public string $username = "";
 public $password;
 public $password_confirmation;
 public $token;

 protected function rules()
 {
  return [
   'username' => 'required',
   'password' => [
    'required',
    'confirmed',
    RulePassword::defaults()
   ],
   'password_confirmation' => 'required',
   'token' => 'required',
  ];
 }

 protected $validationAttributes = [
  'username' => 'ایمیل یا شماره موبایل'
 ];

 public function render()
 {
  return view('livewire.front.auth.forget-password');
 }

 public function sendToken()
 {
  $this->validateOnly('username');
  $status = Password::sendResetLink([
                                     $this->getColumnName() => $this->username,
                                    ], function ($user, $token) {
   /**
    * @var User $user
    */
   if ($this->getColumnName() === 'email') {
    Mail::to($user)
        ->send(new \App\Mail\ForgetPassword($token));
   }
   elseif ($this->getColumnName() === 'phone') {
    $t = new Api(st()->botApiToken);
    $text = 'در صورتی که رمز عبور خود را فراموش کرده اید رمز زیر را برای بازیابی رمز عبورتان وارد نمایید.';
    $t->sendMessage([
                     'chat_id' => $user->telegram_id,
                     'text' => $text,
                    ]);
    $t->sendMessage([
                     'chat_id' => $user->telegram_id,
                     'text' => $token,
                    ]);
   }
  });
  if ($status === Password::RESET_LINK_SENT) {
   $this->dispatchBrowserEvent('swal:modal', [
    'icon' => 'success',
    'title' => $this->isEmail($status) ? __($status) : __('passwords.sent_to_telegram'),
    'timerProgressBar' => true,
    'timer' => 30000,
    'confirmButtonText' => '<i class="fa fa-thumbs-up"></i> متوجه شدم',
    'width' => 300
   ]);
  }
  else {
   $this->dispatchBrowserEvent('swal:modal', [
    'icon' => 'error',
    'title' => __($status),
    'timerProgressBar' => true,
    'timer' => 30000,
    'confirmButtonText' => '<i class="fa fa-thumbs-up"></i> متوجه شدم',
    'width' => 300
   ]);
  }
 }

 public function store()
 {
  $this->validate();
  $status = Password::reset([
                             $this->getColumnName() => $this->username,
                             'password' => $this->password,
                             'token' => $this->token
                            ], function ($user, $password) {
   $user->forceFill([
                     'password' => Hash::make($password)
                    ])
        ->setRememberToken(Str::random(60));
   $user->save();
   event(new PasswordReset($user));
  });
  if ($status === Password::PASSWORD_RESET) {
   $this->dispatchBrowserEvent('swal:modal', [
    'icon' => 'success',
    'title' => __($status),
    'timerProgressBar' => true,
    'timer' => 30000,
    'confirmButtonText' => '<i class="fa fa-thumbs-up"></i> متوجه شدم',
    'width' => 300
   ]);
   $this->backToLogin();
  }
  else {
   $this->dispatchBrowserEvent('swal:modal', [
    'icon' => 'error',
    'title' => __($status),
    'timerProgressBar' => true,
    'timer' => 30000,
    'confirmButtonText' => '<i class="fa fa-thumbs-up"></i> متوجه شدم',
    'width' => 300
   ]);
  }
 }

 public function toggleFormForgetPassword()
 {
  $this->showForm = !$this->showForm;
 }

 public function backToLogin()
 {
  $this->emit('toggleFormLogin');
  $this->toggleFormForgetPassword();
 }

 protected function getColumnName(): string
 {
  $columnName = \Validator::make(['username' => $this->username], [
   'username' => 'email',
  ])
                          ->passes() ? 'email' : 'phone';
  return $columnName;
 }

 protected function isEmail(string $status): bool
 {
  return $status === 'passwords.sent' && $this->getColumnName() === 'email';
 }
}

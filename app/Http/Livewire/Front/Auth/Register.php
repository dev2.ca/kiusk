<?php

namespace App\Http\Livewire\Front\Auth;

use App\Models\User;
use Illuminate\Validation\Rules\Password;
use Livewire\Component;

class Register extends Component
{
 public $email;
 public $phone;
 public $password;
 public $countryCode;

// public $password_confirmation;
 protected function rules()
 {
  return [
   'email' => 'required|email|unique:users,email',
   'phone' => 'required|numeric|unique:users,phone|regex:/^[1-9][0-9]*$/',
   'password' => [
    'required',
    Password::defaults()
//   'confirmed'
   ],
  ];
 }

 protected $messages = [
  'phone.regex' => 'شماره تلفن بدون صفر شروع شود .مانند:9123456789',
 ];

 public function updated($propertyName)
 {
  $this->validateOnly($propertyName);
 }

 public function register()
 {
  $this->validate();
  $user = User::create([
                        'email' => $this->email,
                        'phone' => $this->phone,
                        'country_code' => $this->countryCode,
                        'password' => bcrypt($this->password),
                       ]);
  auth()->login($user);
  $this->dispatchBrowserEvent('swal:modal', [
   'icon' => 'success',
   'title' => 'با موفقیت ثبت نام شدید.',
   'timerProgressBar' => true,
   'timer' => 1000,
   'confirmButtonText' => '<i class="fa fa-thumbs-up"></i> متوجه شدم',
   'width' => 300
  ]);
  return redirect()->intended(route('front.panel.user.ad.index'));
 }

 public function render()
 {
  return view('livewire.front.auth.register');
 }
}

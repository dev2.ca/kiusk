<?php

namespace App\Http\Livewire\Front\Blog;

use App\Models\Blog\Comment;
use App\Models\Blog\Post;
use App\Models\User;
use DB;
use Livewire\Component;

class Show extends Component
{
 public Post $post;
 public $content;
 public $name;
 public $email;
 public $site;
 protected $rules = [
  'content' => 'required|unique:blog_comments,content',
  'name' => 'required|alpha',
  'email' => 'email|required',
  'site' => 'nullable|url'
 ];
// protected $validationAttributes = [
//  'comment.content' => 'متن',
// ];
 protected $listeners = [
  'viewed'
 ];

 public function render()
 {
  return view('livewire.front.blog.show');
 }

 public function viewed()
 {
  DB::table($this->post->getTable())
    ->where('id', $this->post->id)
    ->update(['views' => $this->post->views + 1]);
 }

 public function storeComment()
 {
  $this->validate();
  $user = User::firstOrCreate(['email' => $this->email,], [
   'name' => $this->name,
   'extra' => [
    'site' => $this->site,
   ],
   'rule' => 'subscriber'
  ]);
  $this->post->comments()
             ->create([
                       'user_id' => $user->id,
                       'is_visible' => false,
                       'content' => $this->content,
                      ]);
  $this->dispatchBrowserEvent('swal:modal', [
   'icon' => 'success',
   'title' => 'دیدگاه شما با موفقیت ثبت.',
   'timerProgressBar' => true,
   'timer' => 20000,
   'confirmButtonText' => '<i class="fa fa-thumbs-up"></i> متوجه شدم',
   'width' => 300
  ]);
  $this->reset('content', 'name', 'email', 'site');
 }
}

<?php

namespace App\Http\Livewire\Front\Panel\User\Ad;

use App\Http\Livewire\Front\Panel\User\Ad\Edit\Media;
use App\Models\Ad\Ad;
use App\Models\Ad\AdAttribute;
use Livewire\Component;
use Livewire\WithFileUploads;

class Edit extends Component
{
 use WithFileUploads, Media;

 public function render()
 {
  return view('livewire.front.panel.user.ad.edit');
 }

 protected $rules = [
  'ad.title' => 'required|string|min:3',
  'ad.content' => 'required|string|min:3',
  'ad.price' => 'required|numeric',
  'ad.state_id' => 'required|numeric',
  'ad.city_id' => 'required|numeric',
//  'photos.*' => 'image|max:1024',
//  'photos' => 'array|max:10',
 ];
 protected $validationAttributes = [
  'ad.price' => 'قیمت',
  'ad.state_id' => 'استان',
  'ad.city_id' => 'شهر',
  'photos.*' => 'فایل'
 ];
 public Ad $ad;
 public array $formAttributes = [];
 public $content;

 public function mount($ad)
 {
  /**
   * @var Ad $ad
   * */
  $this->getPhoto();
  $list = [];
  foreach ($this->ad->attrs->toArray() as $item) {
   $item['text'] = $item['pivot']['text'];
   $list[] = $item;
  }
  $this->formAttributes = $list;
  $this->showEmail = $ad?->extra?->showEmail;
  $this->content = strip_tags($ad->content_strip);
 }

 public bool $showEmail = false;
 public function update()
 {
//  dump($this->formAttributes);
//
  foreach ($this->formAttributes as $attribute) {
//   $ad->attrs()->attach([
//    $attribute['id']=>['text'=>$attribute['text']]
//                        ]);
   AdAttribute::whereAdId($this->ad->id)
              ->whereAdAttributeId($attribute['id'])
              ->update(['text' => $attribute['text']]);
  }
  $this->ad->update($this->ad->toArray());
  $stdClass = new \stdClass();
  $stdClass->showEmail = $this->showEmail;
  $this->ad->extra = $stdClass;
  $this->ad->is_visible = false;
  $this->ad->content = $this->content;
  $this->ad->save();
  $this->dispatchBrowserEvent('swal:modal', [
   'icon' => 'success',
   'title' => 'آگهی با موفقیت ویرایش شد.',
   'timerProgressBar' => true,
   'timer' => 20000,
   'confirmButtonText' => '<i class="fa fa-thumbs-up"></i> متوجه شدم',
   'width' => 300
  ]);
 }
}

<?php

namespace App\Http\Livewire\Front\Panel\User\Ad;

use App\Models\Ad\Ad;
use Livewire\Component;
use Spatie\ResponseCache\Facades\ResponseCache;

class Index extends Component
{
 public function render()
 {
  return view('livewire.front.panel.user.ad.index');
 }

 protected $listeners = [
  'delete'
 ];

 public function delete($event, $params)
 {
  if ($event['isConfirmed']) {
   Ad::whereUserId(auth()->id())
     ->whereId($params[0])
     ->delete();
   ResponseCache::clear();
   $this->dispatchBrowserEvent('swal:modal', [
    'icon' => 'success',
    'title' => 'آگهی با موفقیت حذف شد.',
    'timerProgressBar' => true,
    'timer' => 20000,
    'confirmButtonText' => '<i class="fa fa-thumbs-up"></i> متوجه شدم',
    'width' => 300
   ]);
   return $this->redirect(route('front.panel.user.ad.index'));
  }
 }

 public function beforeDelete($id)
 {
  $this->dispatchBrowserEvent('swal:confirm3', [
   'icon' => 'error',
   'title' => 'آیا مایل به حذف آگهی خود هستید؟',
   'timerProgressBar' => true,
   'timer' => 20000,
   'confirmButtonText' => ' حذف',
   'cancelButtonText' => ' صرف نظر',
   'showCancelButton' => true,
   'width' => 300,
   'event' => 'delete',
   'params' => [$id]
  ]);
 }
}
<?php

namespace App\Http\Livewire\Front\Panel\User\Profile\Edit;
trait Media
{
 public $avatar;
 public $previewAvatar;

 public function updatedAvatar($v)
 {
  $this->validate([
                   'avatar' => 'image|max:1024',
                  ], [], [
                   'avatar' => 'عکس پروفایل'
                  ]);
  $user = auth()->user();
  if ($user->getFirstMedia('profile')) {
   $user->getFirstMedia('profile')
        ->delete();
  }
  $media = $user->addMedia($this->avatar)
                ->toMediaCollection('profile');
  $this->previewAvatar = $media->getUrl('avatar');
  $this->dispatchBrowserEvent('swal:modal', [
   'icon' => 'success',
   'title' => 'عکس پروفایل ویرایش شد.',
   'timerProgressBar' => true,
   'timer' => 20000,
   'confirmButtonText' => '<i class="fa fa-thumbs-up"></i> متوجه شدم',
   'width' => 300
  ]);
 }

 public function mediaDelete()
 {
  auth()
   ->user()
   ->getFirstMedia('profile')
   ->delete();
  $this->reset('avatar', 'previewAvatar');
  $this->dispatchBrowserEvent('swal:modal', [
   'icon' => 'success',
   'title' => 'عکس پروفایل حذف شد.',
   'timerProgressBar' => true,
   'timer' => 20000,
   'confirmButtonText' => '<i class="fa fa-thumbs-up"></i> متوجه شدم',
   'width' => 300
  ]);
 }
}
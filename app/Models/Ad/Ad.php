<?php

namespace App\Models\Ad;

use App\Models\Lib\ClearsResponseCache;
use App\Models\Address\City;
use App\Models\Address\State;
use App\Models\Payment\Payment;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Tags\HasTags;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

class Ad extends Model implements HasMedia, Sortable
{
 use HasFactory;
 use InteractsWithMedia;
 use HasTags;
 use ClearsResponseCache;
 use SortableTrait;

 protected $fillable = [
  'title',
  'slug',
  'content',
  'content_html',
  'excerpt',
  'is_visible',
  'price',
  'is_visible_email',
  'phone',
  'email',
  'seo_title',
  'seo_description',
  'views',
  'extra',
  'user_id',
  'state_id',
  'city_id',
  'created_at',
  'updated_at'
 ];
 protected $casts = [
  'is_visible' => 'boolean',
  'is_visible_email' => 'boolean',
  'extra' => 'json',
 ];

 public function tags(): MorphToMany
 {
  return $this->morphToMany(self::getTagClassName(), 'taggable', 'taggables', null, 'tag_id')
              ->orderBy('order_column');
 }

 public function user(): BelongsTo
 {
  return $this->belongsTo(User::class);
 }

 public function state(): BelongsTo
 {
  return $this->belongsTo(State::class);
 }

 public function city(): BelongsTo
 {
  return $this->belongsTo(City::class);
 }

 public function categories(): BelongsToMany
 {
  return $this->belongsToMany(Category::class, 'ad_category_pivot', 'ad_id', 'ad_category_id')
              ->withPivot('is_main')
              ->withTimestamps();
 }

 public function mainCategory()
 {
  return $this->belongsToMany(Category::class, 'ad_category_pivot', 'ad_id', 'ad_category_id')
              ->wherePivot('is_main', 1);
 }

 public function favorites(): HasMany
 {
  return $this->hasMany(Favorite::class);
 }

 public function reports(): HasMany
 {
  return $this->hasMany(Report::class);
 }

 public function reviews(): HasMany
 {
  return $this->hasMany(Review::class);
 }

 public function attrs(): BelongsToMany
 {
  return $this->belongsToMany(Attribute::class, 'ad_attribute_pivot', 'ad_id', 'ad_attribute_id')
              ->withPivot('text', 'boolean', 'integer', 'float', 'date_time', 'date', 'json',)
              ->withTimestamps();
 }

 public function attrs2(): HasMany
 {
  return $this->hasMany(AdAttribute::class);
 }

// public function specialImage()
// {
//  return $this->media()
//              ->first();
// }
 public function registerMediaConversions(Media $media = null): void
 {
  $this->addMediaCollection('SpecialImage')
//   ->useFallbackUrl('https://www.google.com/images/branding/googlelogo/1x/googlelogo_light_color_272x92dp.png');
       ->useFallbackPath(public_path('/images/hero-background-icons (1).jpg'));
//  $this->addMediaConversion('thumb0')
//       ->fit(Manipulations::FIT_CROP, 400, 333)
//       ->performOnCollections('Gallery', 'SpecialImage');
//  $this->addMediaConversion('thumb1')
//       ->fit(Manipulations::FIT_CONTAIN, 400, 333)
//       ->performOnCollections('Gallery', 'SpecialImage');
//  $this->addMediaConversion('thumb2')
//       ->fit(Manipulations::FIT_FILL, 400, 333)
//       ->performOnCollections('Gallery', 'SpecialImage');
//  $this->addMediaConversion('thumb3')
//       ->fit(Manipulations::FIT_MAX, 400, 333)
//       ->performOnCollections('Gallery', 'SpecialImage');
//  $this->addMediaConversion('thumb4')
//       ->fit(Manipulations::FIT_STRETCH, 400, 333)
//       ->performOnCollections('Gallery', 'SpecialImage');
  $this->addMediaConversion('thumb')
//   ->useFallbackUrl('/images/anonymous-user.jpg')
//   ->useFallbackPath(public_path('/images/anonymous-user.jpg'))
       ->crop(Manipulations::CROP_CENTER, 400, 333)
       ->performOnCollections('SpecialImage');
  $this->addMediaConversion('70_70')
       ->crop(Manipulations::CROP_CENTER, 70, 70)
       ->performOnCollections('SpecialImage');
 }

 public function getShortLinkAttribute()
 {
  $id = null;
  if (isset($this->extra->wordpressId)) {
   $id = $this->extra->wordpressId;
  }
  else {
   $id = $this->id;
  }
  return route('front.home', ['p' => $id]);
 }

 public function setExtraAttribute($value)
 {
  if ($value) {
   $this->attributes['extra'] = json_encode($value);
  }
  else {
   $this->attributes['extra'] = json_encode([]);
  }
 }

 public function getExtraAttribute()
 {
  if (isset($this->attributes['extra'])) {
   return json_decode($this->attributes['extra']);
  }
//  return new \stdClass();
 }

 public function getSeoTitleReadyAttribute()
 {
  $list = [
   '%%sep%%' => '|',
   '%%sitename%%' => 'کیوسک',
   '%%title%%' => ''
  ];
  foreach ($list as $k => $v) {
   if ($k == '%%title%%' && !$v) {
    $v = $this->attributes['title'];
   }
   $value = str_replace($k, $v, $value);
  }
  return $value;
 }

 public function payments(): MorphMany
 {
  return $this->morphMany(Payment::class, 'payable');
 }

 public function getContentAttribute($value)
 {
  $stringWithPs = str_replace("\n\n", "</p><p>", $value);
  $stringWithPs = str_replace("\r\n", "</p><p>", $stringWithPs);
  $stringWithPs = str_replace("\n", "</p><p>", $stringWithPs);
  $stringWithPs = "<p>" . $stringWithPs . "</p>";
  $stringWithPs = str_replace("<p></p>", "", $stringWithPs);
  foreach ([
            'h1',
            'h2',
            'h3',
            'h4',
            'h5',
            'h6',
            'li',
            'ul',
            'ol',
            'p'
           ] as $item) {
   $stringWithPs = str_replace("<p><" . $item, "<" . $item, $stringWithPs);
   $stringWithPs = str_replace("</" . $item . "></p>", "</" . $item . ">", $stringWithPs);
  }
  return $stringWithPs;
 }

 public function getContentStripAttribute()
 {
  return strip_tags($this->original['content']);
 }
}

<?php

namespace App\Models\Blog;

use App\Models\Lib\ClearsResponseCache;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Tags\HasTags;

class Category extends Model implements HasMedia
{
 use HasFactory;
 use ClearsResponseCache;
 use InteractsWithMedia;
 use HasTags;

 /**
  * @var string
  */
 protected $table = 'blog_categories';
 /**
  * @var array<int, string>
  */
 protected $fillable = [
  'name',
  'slug',
  'description',
  'seo_title',
  'seo_description',
  'is_visible',
 ];
 /**
  * @var array<string, string>
  */
 protected $casts = [
  'is_visible' => 'boolean',
 ];

 public function posts(): HasMany
 {
  return $this->hasMany(Post::class);
 }
}

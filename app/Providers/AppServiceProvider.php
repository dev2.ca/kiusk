<?php

namespace App\Providers;

use App\Models\Ad\Ad;
use App\Models\Ad\Category;
use App\Models\Address\City;
use App\Models\Address\State;
use App\Models\Blog\Post;
use App\Models\User;
use Filament\Facades\Filament;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Illuminate\Validation\Rules\Password;
use SEO;

class AppServiceProvider extends ServiceProvider
{
 /**
  * Register any application services.
  * @return void
  */
 public function register()
 {
 }

 /**
  * Bootstrap any application services.
  * @return void
  */
 public function boot()
 {
  Filament::registerNavigationGroups([
                                      'Ads',
                                      'Blog',
                                      'Payment'
                                     ]);
  Filament::registerStyles([
                            asset('css/admin/style.css'),
                           ]);
  Filament::registerScripts([
                             asset('js/jquery.min.js'),
                             asset('js/admin/app.js'),
                            ]);
  Relation::enforceMorphMap([
                             'post' => Post::class,
                             'postCategory' => \App\Models\Blog\Category::class,
                             'ad' => Ad::class,
                             'adCategory' => Category::class,
                             'user' => User::class,
                             'state' => State::class,
                             'city' => City::class,
                            ]);
  Password::defaults(function () {
   return $rule = Password::min(st()->profileKeyboard[2]['keyRule'])
//    ->letters()
//    ->mixedCase()
//    ->numbers()
//    ->symbols()
//    ->uncompromised()
    ;
//   return $this->app->isProduction()
//    ? $rule->mixedCase()->uncompromised()
//    : $rule;
  });
 }
}

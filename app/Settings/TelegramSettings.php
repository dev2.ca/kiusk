<?php

namespace App\Settings;

use App\Settings\JsonArray;
use Spatie\LaravelSettings\Settings;

class TelegramSettings extends Settings
{
 public string $botApiToken;
 public string $startImage;
 public string $startText;
 public $startKeyboard;
 public string $registerText;
 public string $registerSuccess;
 public $registerKeyboard;
 public string $profileText;
 public $profileKeyboard;
 public string $adsCreateText;
 public string $adsCreateSuccess;
 public $adsCreateKeyboard;
 public string $adsEditText;
 public string $adsEditSuccess;
 public $adsEditKeyboard;
 public string $adsListText;
 public string $adsListTextEmpty;
 public string $adsListIsNotVisible;
 public string $adsListBack;
 public string $adsAcceptTheRulesKeyName;
 public string $adsAcceptTheRulesText;

 public static function group(): string
 {
  return 'telegram';
 }

 public static function casts(): array
 {
  return [
   'adsEditKeyboard' => JsonArray::class,
   'adsCreateKeyboard' => JsonArray::class,
   'profileKeyboard' => JsonArray::class,
   'registerKeyboard' => JsonArray::class,
   'startKeyboard' => JsonArray::class,
  ];
 }
}

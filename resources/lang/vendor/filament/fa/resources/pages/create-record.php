<?php
return [
 'title' => ' :labelتازه ',
 'breadcrumb' => 'ايجاد كردن',
 'form' => [
  'actions' => [
   'cancel' => [
    'label' => "لغو",
   ],
   'create' => [
    'label' => 'ذخیره',
   ],
   'create_and_create_another' => [
    'label' => "ذخیره و ایجاد دیگری",
   ],
  ],
 ],
 'messages' => [
  'created' => 'ایجاد شده',
 ],
];

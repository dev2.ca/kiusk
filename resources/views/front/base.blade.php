<!DOCTYPE html>
<html lang="fa"
      dir="rtl">
<head>
 {!! SEO::generate() !!}
 <link rel="icon"
       type="image/png"
       href="{{asset('storage/'.s()->favicon)}}">
 @include('front.layouts.head')
 @yield('head')
 @include('front.layouts.Seo')
 @yield('seo')
</head>
<body>
<header class="bg-white header"
        id="header">
 @include('front.layouts.header.header')
</header>
<!-- header -->
<!-- main -->
<main @class(['content' ,'main-color'=>request()->routeIs('front.ad.show')])>
 @yield('content')
</main>
<!-- footre -->
<footer class="footer">
 @include('front.layouts.footer')
</footer>
@include('front.layouts.returnToTop')
<!-- modal -->
@include('front.layouts.modal-category.modal-category2')
<!-- script -->
@include('front.layouts.script')
@livewireScripts
<livewire:front.sweet-alert/>
@yield('script')
</body>
</html>

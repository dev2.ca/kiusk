<div class="bg-dark">
 <div class="container">
  <nav class="navbar navbar-expand-lg navbar-light bg-dark p-0">
   <div class="container-fluid">
    <button class="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown"
            aria-expanded="false"
            aria-label="Toggle navigation">
     <span class=""><i class="fas fa-bars text-white"></i></span>
    </button>
    <div class="collapse navbar-collapse"
         id="navbarNavDropdown">
     <ul class="navbar-nav pt-1 pb-1">
      <li class="nav-item">
       @auth
        <a href="{{route('front.panel.user.profile.edit')}}"
           class="nav-link text-white font">
         <i class="fa fa-user text-secondary"></i> خوش آمدید {{auth()->user()->name}} </a>
        <ul class="inner-ul p-0 profile">
         <li class="nav-item"><a href="{{route('front.panel.user.ad.index')}}"><i class="fas fa-bullhorn m-1"></i>اگهی
           های من</a></li>
         <li class="nav-item"><a href="{{route('front.panel.user.favorite.index')}}"><i class="fa fa-bookmark m-1"></i>علاقه‌مندی
           ها</a></li>
         <li class="nav-item">
          <a href="{{route('front.panel.user.payment.index')}}"><i class="far fa-credit-card m-1"></i>پرداخت های من</a>
         </li>
         <li class="nav-item"><a href="{{route('front.panel.user.profile.edit')}}"><i class="fa fa-edit m-1"></i>اطلاعات
           کاربری</a></li>
         <li class="nav-item"><a href="{{route('front.panel.user.profile.show')}}"><i class="fa fa-user m-1"></i>نمایش
           پروفایل</a></li>
         @livewire('front.auth.logout')
        </ul>
       @endauth
       @guest
        <a href="{{route('front.panel.user.ad.index')}}"
           class="nav-link active text-white font">
         <i class="fa fa-sign-in text-secondary"></i> ورود / ثبت نام </a>
       @endguest
      </li>
      @foreach(s()->headerBlackMenu as $item)
       <li class="nav-item">
        <a href="{{$item['url']}}"
           class="nav-link text-white font">
         <i class="{!! $item['icon'] !!} text-secondary"></i> {!! $item['text'] !!} </a>
       </li>
      @endforeach
     </ul>
    </div>
   </div>
  </nav>
 </div>
</div><!-- main navigation -->

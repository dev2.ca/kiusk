<div class="main-navigation">
 <div class="container ">
  <nav class="navbar navbar-expand-lg navbar-light border-0">
   @include('front.layouts.header.menu')
  </nav>
 </div>
 <p class="d-flex justify-content-between col-11 col-md-12 border-top container colpas-button">
  <span class="mt-2 text-title">
   <a href="{{route('front.home')}}">خانه</a>
   @switch(request()->route()->getName())
    @case('front.rules')
    / قوانین و مقررات
    @break
    @case('front.contact-us')
    / تماس با ما
    @break
    @case('front.ad.category.index.first.page')
    @case('front.ad.category.index')
    @php
     $class=new App\Services\AdCategory\AdCategory();
    @endphp
    @foreach($class->categoryAddress(request()->category_page) as $category)
     @if($loop->last)
      @if(request()->page && request()->page >1)
       /  <a href="{{route('front.ad.category.index.first.page',$category->slug)}}"> {{$category->name}}</a> /
       برگه {{request()->page}}

      @else
       /  {{$category->name}}
      @endif
     @else
      / <a href="{{route('front.ad.category.index.first.page',$category->slug)}}"> {{$category->name}}</a>
     @endif
    @endforeach
    @break
    @case('front.ad.tag.index.first.page')
    /
    محصولات برچسب خورده “ {{request()->tag_page->name}}”
    @break
    @case('front.ad.tag.index')
    /  <a href="{{route('front.ad.tag.index.first.page',request()->tag_page->slug)}}">
     محصولات برچسب خورده “ {{request()->tag_page->name}}”
    </a> /
    برگه {{request()->page}}

    @break
    @case('front.ad.index')
    @if(request()->page && request()->page >1)
     <a href="{{route('front.ad.index')}}">/ آگهی ها</a>
     / برگه {{request()->page}}
    @else
     / آگهی ها
    @endif
    @break
    @case('front.ad.show')
    @php
     $class=new App\Services\AdCategory\AdCategory();
    @endphp
    @if(count(request()->ad->mainCategory))
     @foreach($class->categoryAddress(request()->ad->mainCategory[0]) as $category)
      / <a href="{{route('front.ad.category.index.first.page',$category->slug)}}"> {{$category->name}}</a>
     @endforeach
    @endif
    / {{request()->ad->title}}
    @break
    @case('front.blog.show')
    <?php
    /**
     * @var \App\Models\Blog\Post $post
     * */
    $post = request()->post
    ?>
    / <a href="{{route('front.blog.category.blog.index.first.page')}}">{{$post->category->name}}</a>
    / {{$post->title}}
    @break
    @case('front.panel.user.profile.show')
    / پروفایل
    @break
   @endswitch
  </span>
  <button class="btn btn-primary search-icon-btn"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#collapseExample"
          aria-expanded="false"
          aria-controls="collapseExample">
   <i class="far fa-search text-white"></i>
   <i class="fas fa-times text-white"></i>
  </button>
 </p>
 <div class="collapse"
      id="collapseExample">
  @livewire('front.ad.search')
 </div>
 <div class="container p-3 pt-4 p-lg-0 pt-lg-4 pb-lg-5
            pb-5"
      style="z-index: 100">
  {{--  <h1 id="headerTitle">آخرین آگهی‌ها</h1>--}}
  @switch(request()->route()->getName())
   @case('front.rules')
   <a href="{{route('front.rules')}}"><h1 id="headerTitle"> قوانین و مقررات</h1></a>
   @break
   @case('front.contact-us')
   <a href="{{route('front.contact-us')}}"><h1 id="headerTitle">تماس با ما</h1></a>
   @break
   @case('front.ad.category.index.first.page')
   @case('front.ad.category.index')
   <h1 id="headerTitle">
    {{ request()->category_page->name .' |'}}
    @if(request()->page &&  request()->page>1)
     صفحه {{request()->page}} از {{request()->total_page}} |
    @endif
    {{s()->headerTextClose}}
   </h1>
   @break
   @case('front.ad.show')
   <?php
   /**
    * @var App\Models\Ad\Ad $ad
    * */
   $ad = request()->ad
   ?>
   <a href="{{route('front.ad.show',$ad->slug)}}"><h1 id="headerTitle">{{$ad->title}}</h1></a>

   <div class="d-flex justify-content-between pt-2">
    <span class="text-secondary"><i class="fal fa-map-marker-alt m-1"></i>
     @if($ad?->state)
      <a href="{{route('front.ad.category.city.index.first.page',$ad?->state->slug)}}">
       {{$ad?->state->name}}
      </a>
     @elseif($ad?->state && $ad?->city)
      <i class="fa fa-angle-left"
         aria-hidden="true"></i>
     @elseif( $ad?->city)
      <a href="{{route('front.ad.category.city.index.first.page',$ad?->city->slug)}}">
       {{$ad?->city->name}}
      </a>
     @endif
    </span>
    <h4 class="text-secondary"><a class="text-secondary">تماس بگیرید</a></h4>
   </div>
   @break
   @case('front.blog.show')
   <?php
   /**
    * @var \App\Models\Blog\Post $post
    * */
   $post = request()->post
   ?>
   <a href="{{$post->link}}"><h1 id="headerTitle">{{$post->title}}</h1></a>
   <div class="d-flex justify-content-between pt-2">
    <div class="text-secondary d-flex details align-items-center">
     <h5><i class="far fa-bookmark"></i>
      {{$post->category->name}}
     </h5>
     <span class="ms-4">
      <i class="fa fa-calendar-o"></i> {{jdate($post->created_at)->ago()}}</span>
     </span><span><i class="fa fa-pie-chart"
                     aria-hidden="true"></i> {{$post->views}} بازدید</span>
    </div>
   </div>
   @break
   @case('front.panel.user.profile.show')
   <a href="{{route('front.panel.user.profile.show')}}"><h1 id="headerTitle">پروفایل</h1></a>
   @break
  @endswitch
 </div>
 <div class="background"
      style=";z-index: -10">
  <div class="background-image original-size"
       style="background-image: url({{asset('images/hero-background-icons (1).jpg')}})">
   <img src="{{asset('storage/'.s()->headerBackgroundImage)}}"
        alt="{{s()->headerText}}"
        style=";z-index: -10">
  </div>
 </div>
</div>
<div class="after_header"></div>

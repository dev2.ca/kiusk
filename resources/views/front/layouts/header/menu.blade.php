<a class="navbar-brand"
   href="{{route('front.home')}}">
 <img src="{{asset('storage/'.s()->logo)}}">
</a>
<button class="navbar-toggler border-0"
        type="button"
        data-bs-toggle="collapse"
        data-bs-target="#navbar"
        aria-controls="navbar"
        aria-expanded="false"
        aria-label="Toggle navigation">
 <span class="navbar-toggler-icon bg-white"></span>
</button><!-- navigation -->
<div class="navbar navbar-expand-lg navbar-light navigation collapse navbar-collapse"
     id="navbar">
 <ul class="navbar-nav">
  <?php
  $path = urldecode('/' . request()->path());
  if (request()->routeIs('front.home') && $path === '//') {
   $path = '/';
  }
  $showedCategory = false;
  ?>
  @foreach(s()->headerMainMenu as $item)
   @if($loop->iteration==s()->sequenceCategoryMenu)
    @include('front.layouts.header.category-menu.category-menu')
    <?php
    $showedCategory = true;
    ?>
   @endif
   <li class="nav-item">
    <a href="{{$item['url']}}"
       class="nav-link {{$path == urldecode($item['url'])?'active-link-primary':''}}">{!! $item['text'] !!}</a></li>

  @endforeach
  @includeWhen(!$showedCategory,'front.layouts.header.category-menu.category-menu')
 </ul>
</div><!-- buttons -->
<ol class="nav-btn">
 <li class="top-category-btn">
  <button type="button"
          class="btn btn-primary text-caps btn-rounded btn-framed"
          data-bs-toggle="offcanvas"
          data-bs-target="#offcanvasRight"
          aria-controls="offcanvasRight">
   <i class="fa fa-align-justify me-1"
      aria-hidden="true"></i>
   دسته‌بندی‌ها
  </button>
  <!--  -->
 </li>
 <li class="nav-item submit_ad">
  <a href="{{route('front.ad.create')}}"
     class="btn btn-primary text-caps btn-rounded btn-framed">
   <i class="fa fa-plus me-1"
      aria-hidden="true"></i> ثبت آگهی </a>
 </li>
</ol>

@extends('front.base')
@section('seo')
@endsection
@section('content')
 <section class=" blog-block m-0 p-4">
  <div class="container border-0 border-bottom">
   <livewire:front.ad.advance-search/>
   <livewire:front.ad.search.list-search :ads="$ads"
                                         :urls="$urls"/>
  </div>
 </section>

@endsection
@extends('front.base')
@section('seo')
@endsection
@section('content')
 <section class=" blog-block m-0 pt-5">
  <div class="container">
   <article>
    <div class="row justify-content-center justify-content-md-between pt-4">
     <div class="col-4 hidden">
      <div class="mb-3 rounded bg-white shadow-2">
       <div class="p-2">
        <h5 class="text-center border-bottom p-2">آخرین ها</h5>
        <ul class="pe-2 ps-2 list-style-type">
         @php
          $latestPosts=\App\Models\Blog\Post::latest()->limit(s()->numberPostsSidebarIndexBlogPage)->get();
         @endphp
         @foreach($latestPosts as $key=>$post0)
          <li>
           <a href="{{$post0->link}}">{{$post0->title}}</a>
          </li>
         @endforeach
        </ul>
       </div>
      </div>
      <div class="mb-3 rounded bg-white shadow-2">
       <div class="p-2 pe-3">
        <!--به تگ aدر li ها name اضاقه کردن کلاس  -->
        <ul class="p-0">
         @php
          $ads=\App\Models\Ad\Ad::with(['user', 'media' => function ($q) {
                    $q->whereCollectionName('SpecialImage');
                   },])->whereIsVisible(true)->latest()->limit(s()->numberAdsSidebarIndexBlogPage)->get();
         @endphp
         @foreach($ads as $key=>$ad)
          <li @class([
        'border-bottom'=> !$loop->last
])>
           <div class="d-flex align-items-start justify-content-between">
            <div class="ms-1">
             <a class="name"
                href="{{route('front.ad.show',[$ad->slug])}}">{{$ad?->title}}</a>
             <p class="text-success text-little">تماس بگیرید</p>
            </div>
            <div class="p-2 pe-0 pb-0">
             <a href="{{route('front.ad.show',[$ad->slug])}}">
              <img src="{{$ad?->getFirstMedia('SpecialImage')?->getUrl('70_70')}}"
                   alt=""
                   width="55px"
                   height="55px"
                   class="border">
             </a>
            </div>
           </div>
          </li>

         @endforeach
        </ul>
       </div>
      </div>
     </div>
     <!--  -->
     <div class="col-10 col-md-8">
      <div>
       @forelse($posts as $post)
        <article class="rounded shadow-2 d-flex p-md-4 mb-3 bg-white flex-wrap flex-lg-nowrap">
         <a class="col-12 col-md-4 d-inline-block rounded position-relative"
            href="{{$post->link}}">
          <img src="{{$post?->getFirstMedia('SpecialImage')?->getUrl('150_150')}}"
               alt=""
               class="poster rounded"
               width="220px"
               height="170px">
          <span class="date">{{jdate($post->created_at)->format('j F')}}</span>
         </a>
         <div class="ms-0 ms-md-0 p-2 pt-3 p-lg-0 article-para">
          <a href="{{$post->link}}">
           <h2 class="text-secondary card_blog_title">{{$post->title}}</h2></a>
          <p class="text-secondary">{!!  $post->limit_content !!}</p>
          <a href="{{$post->link}}"
             class="text-success text-little">بیشتر بخوانید</a>
         </div>
        </article>
       @empty
        <article class="rounded shadow-2 d-flex p-md-4 mb-3 bg-white flex-wrap">
         مقاله ای یافت نشد.
        </article>
       @endforelse
       @if($posts->count())
        <div class="mt-3 w-100 d-flex justify-content-center">
         <nav aria-label="Page navigation example">
          <ul class="pagination">
           @foreach($urls as $url)
            <li class="page-item rounded @if($url['active']) active @endif  @if(!$url['url'] )disabled
@endif"><a class="page-link  bg-transparent"
           href="{{$url['url']}}">{!! $url['label'] !!}</a></li>
           @endforeach
          </ul>
         </nav>
        </div>
       @endif
      </div>
     </div>
    </div>
   </article>
  </div>
 </section>
@endsection

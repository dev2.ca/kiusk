@extends('front.base')
@section('head')
@endsection
@section('seo')
@endsection
@section('content')
 <livewire:front.ad.last-ads/>
 @php
  $posts=\App\Models\Blog\Post::with(['category','media' => function ($q) {
                 $q->whereCollectionName('SpecialImage');
                },])->latest()->limit(s()->numberBlogPostsHomePage)->get()->chunk(4);
 @endphp
 @include('front.pages.home.home.articles',['posts'=>$posts,'title'=>'وبلاگ','css'=>'row-cols-md-4'])
@endsection

@section('script')
 <script>
   $(document).ready(function () {
     $(".more-content").slice(0, {{s()->numberAdsHomePage}}).show();
     $(".loadmore").on("click", function (e) {
       e.preventDefault();
       $(".more-content:hidden").slice(0, 8).slideDown();
       if ($(".more-content:hidden").length == 0) {
         $(".loadmore")
           // .text("وجود ندارد")
           .addClass("noContent");
       }
     });
   })
 </script>
@endsection

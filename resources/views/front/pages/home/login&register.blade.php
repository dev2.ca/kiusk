@extends('front.base')
@section('seo')
@endsection
@section('content')

 <?php
 $newAdAlert = session('url.intended') === route('front.ad.create');
 ?>
 <div class="position-relative">
  <div id="loading_all"
       class="loading ">
   <div class="loader-show"></div>
  </div>
  <section class=" blog-block m-0 pt-5">
   <div @class(['container','pt-4'=>$newAdAlert])>
    @if($newAdAlert)
     <div class="alert alert-warning"
          role="alert">
      <div>
       <h2> برای ارسال آگهی باید وارد سایت شوید</h2>
      </div>
      <div>
       <p class="m-0">ارسال آگهی تنها برای اعضای سایت امکان پذیر می‌باشد.<br>
        چنانچه هم‌ اکنون عضو سایت هستید وارد شوید و در غیر این صورت در سایت ثبت نام کنید </p>
      </div>
     </div>
    @endif
    <div @class(['row','mt-5'=>$newAdAlert])>
     @livewire('front.auth.forget-password')


     @livewire('front.auth.login')

     @livewire('front.auth.register')
    </div>
   </div>
  </section>
 </div>

@endsection
@section('script')
 <script>
   Livewire.on('toggleFormForgetPassword', function () {
     $('#loading_all').addClass('loading_show')
     setTimeout(() => {
       $('#loading_all').removeClass('loading_show')
     }, 1000);
   });
   Livewire.on('toggleFormLogin', function () {
     $('#loading_all').addClass('loading_show')
     setTimeout(() => {
       $('#loading_all').removeClass('loading_show')
     }, 1000);
   })
 </script>
@endsection

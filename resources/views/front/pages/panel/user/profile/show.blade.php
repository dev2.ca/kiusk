@extends('front.base')
@section('seo')
@endsection
@section('content')
 <section class=" blog-block m-0">
  <div class="container p-4">
   <section class="row justify-content-between">
    <div class="col-12 col-md-3">
     <div class="shadow">
      <div class="top-nav-head">
       @php
        $user=auth()->user();
       @endphp
       <img src="{{$user?->getFirstMedia('profile')?->getUrl('avatar')}}"
            alt="{{Str::mask($user->phone,'*',3,4)}}">
       <h3>{{Str::mask($user->phone,'*',3,4)}}</h3>
      </div>
      <ul class="profile mt-3">
       <li><strong><i class="fa fa-phone"
                      aria-hidden="true"></i> شماره تماس: </strong>
        <p><a href="tel:{{$user->phone}}">{{$user->phone}}</a></p>
       </li>
       <li><strong><i class="fa fa-envelope-open"
                      aria-hidden="true"></i> ایمیل: </strong>
        <p><a href="mailto:{{$user->email}}">{{$user->email}}</a></p>
       </li>
      </ul>
     </div>
    </div>
   </section>
  </div>
 </section>
@endsection

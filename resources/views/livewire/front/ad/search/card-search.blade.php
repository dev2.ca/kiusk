<div class="col pe-3 ps-2 pt-2
@if(request()->routeIs('front.home'))more-content
@endif">
 <div class="card">
  @if(count($ad['media']))
   @php
    foreach ($ad['media'] as $item){
if ($item['collection_name'] ==='SpecialImage'){
 $m=new \Spatie\MediaLibrary\MediaCollections\Models\Media($item);
    $src=$m->getUrl('thumb');
}
}
   @endphp
   <a href="{{route('front.ad.show',['slug'=>$ad['slug']])}}">
    <img src="{{$src}}"
         class="card-img-top "
         title="{{$ad['title']}}"
         alt="...">
   </a>

  @endif
  <span class=" bookmark "
        data-toggle="tooltip"
        data-placement="top"
        wire:click="favorite"
        title="افزودن به علاقه مندی ها"><i class="@if($isFavorite)fas

        @else()far
@endif fa-bookmark text-white fs-6"></i></span>
  @if($local)
   <script !src="">
     document.cookie = 'favorites=@json($favorits); expires=Thu, 18 Dec 2045 12:00:00 UTC; path=/';
   </script>
  @endif
{{--  <span class="ad_visit">{{$ad['views']}} بازدید</span>--}}
  @if(isset($ad['state']) || $ad['city'])
   <h4 class="location">
    @if(isset($ad['state']))
     <a class=""
        href="{{route('front.ad.category.city.index.first.page',$ad['state']['slug'])}}">{{$ad['state']['name']}}</a>
    @endif
    @if(isset($ad['city']))
     <a class=""
        href="{{route('front.ad.category.city.index.first.page',$ad['city']['slug'])}}">{{$ad['city']['name']}}</a>
    @endif
   </h4>
  @endif
  <span class="price"><span>تماس بگیرید</span></span>
  <div class="card-body pt-2 pb-0">
   <div class="meta">
    <a href="{{route('front.ad.show',['slug'=>$ad['slug']])}}">
     <h5 class="card-title text-dark"
         title="{{$ad['title']}}">{{$ad['title']}}</h5>
    </a>
    <figure>
     <i class="fa fa-calendar-o"></i> {{jdate($ad['created_at'])->ago()}}
    </figure>
    @if( isset($ad['main_category']) && count($ad['main_category']))
     <figure>
      <i class="fa fa-folder-open"></i><a href="{{route('front.ad.category.index.first.page',$ad['main_category'][0]['slug'])}}"> {{$ad['main_category'][0]['name']}}</a>
     </figure>
    @endif
   </div>
  </div>
 </div>
</div>

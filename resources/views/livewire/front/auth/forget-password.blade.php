<div class="col-12 col-md @if(!$showForm) d-none @endif"
     style="  transition: width 2s, height 4s;">
 <div class="section-title clearfix">
  <h2>فراموشی رمز عبور</h2>
 </div>
 <form action=""
       autocomplete="off"
       class="bg-white p-4 shadow rounded ">
  @error('all') <span class=" text-danger">{{ $message }}</span> @enderror
  <div class="form-floating mb-3">
   <input type="text"
          class="form-control @error('username') is-invalid  @enderror"
          id="floatingInput"
          autocomplete="off"
          wire:model="username"
          placeholder="name@example.com">
   <label for="floatingInput">ایمیل یا شماره موبایل حساب تلگرام بدون صفر *
   </label>
   @error('username') <span class=" text-danger">{{ $message }}</span> @enderror
  </div>
  <div class="input-group mb-3">
   <div class="input-group-prepend">
    <button type="button"
            class="btn   btn-primary"
            style="border-top-left-radius: 0;border-bottom-left-radius: 0"
            wire:click.prevent="sendToken">ارسال کد تایید
    </button>
   </div>
   <input type="text"
          class="form-control @error('password') is-invalid  @enderror"
          autocomplete="off"
          wire:model="token"
          style="margin-top: 0;    height: 53.5px !important;">
  </div>
  @error('token') <span class=" text-danger">{{ $message }}</span> @enderror
  <div class="form-floating">
   <input type="password"
          class="form-control @error('password') is-invalid  @enderror"
          id="floatingPassword"
          autocomplete="new-password"
          wire:model="password"
          placeholder="Password">
   <label for="floatingPassword">کلمه عبور جدید *
   </label>
   @error('password') <span class=" text-danger">{{ $message }}</span> @enderror
  </div>
  <div class="form-floating">
   <input type="password"
          class="form-control @error('password_confirmation') is-invalid  @enderror"
          id="floatingPassword"
          autocomplete="new-password"
          wire:model="password_confirmation"
          placeholder="Password">
   <label for="floatingPassword">تکرار کلمه عبور *
   </label>
   @error('password_confirmation') <span class=" text-danger">{{ $message }}</span> @enderror
  </div>
  <div>
   <button class="btn mt-2 mb-2 btn-primary"
           wire:click.prevent="store">ذخیره
   </button>
   <button class="btn mt-2 mb-2 btn-primary"
           wire:click.prevent="backToLogin">بازگشت
   </button>
  </div>
 </form>
</div>

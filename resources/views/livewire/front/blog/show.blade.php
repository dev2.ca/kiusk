<div class="col-12 col-md-8">
 <div class="bg-white">
  <article class="paragraph">
   <div class="card text-white front-opacity">
    <img src="{{$post->getFirstMediaUrl('SpecialImage')}}"
         class="card-img"
         alt="...">
   </div>
   {{--

   todo must be edit 900
   --}}
   @if($post->id>900)
    <h5 class="card-title mt-4 ms-3">{{$post->title}}</h5>
   @endif
   <div class="p-4 ">
    <div class="ck-content">
     {{--     @dump($post->content)--}}
     {!! $post->content !!}
    </div>
    <div>
     <div class="row justify-content-between">
      @php
       $post3=\App\Models\Blog\Post::oldest()->where('id','>',$post->id)->first();
      @endphp
      @if($post3)
       <div class="col text-start  justify-content-start">
        <!--rounded اضاقه کردن کلاس  -->
        <a class="text-dark"
           href="{{$post3->link}}">
         <div class="bg-light rounded p-3 col-md-12 next_icon_div">
          <i class="fa fa-chevron-right next_icon"></i>
          <span class="text-secondary links text-right">مطلب بعدی</span>
          <p class="text-start mt-2 links">{{$post3->title}}
          </p>
         </div>
        </a>
       </div>
      @endif
      @php
       $post2=\App\Models\Blog\Post::latest()->where('id','<',$post->id)->first();
      @endphp
      @if($post2)

       <div class="col text-end  justify-content-end">
        <a class="text-dark"
           href="{{$post2->link}}">
         <div class="bg-light rounded p-3 col-md-12 before_icon_div">
          <i class="fa fa-chevron-left before_icon"></i>
          <span class="text-secondary links text-left text-center">مطلب قبلی</span>
          <p class="text-start mt-2 links">
           {{$post2->title}}
          </p>
         </div>
        </a>
       </div>
      @endif
     </div>
    </div>
   </div>
  </article>
 </div>
 @php

  $posts=\App\Models\Blog\Post::with(['category','media' => function ($q) {
                 $q->whereCollectionName('SpecialImage');
                },])
                ->whereHas('category',function ($q) use ( $post){
$q->whereId($post->blog_category_id);
})

                ->latest()->limit(s()->numberPostsShowBlogPage)->get()->chunk(3);
 @endphp
 @include('front.pages.home.home.articles',['posts'=>$posts,'title'=>'وبلاگ','css'=>''])
 <div>
  <h3>دیدگاهتان را بنویسید </h3>
  @auth()
   <p>با عنوان {{auth()->user()->name}} وارد شده‌اید. </p>
  @endauth
  @guest()
   <p>برای ارسال دیدگاه ابتدا <b><a href="{{route('front.login-register')}}">وارد</a></b> شوید.</p>

  @endguest
  <form action="">
   <div class="mb-3">
    <label for="exampleFormControlTextarea1"
           class="form-label">دیدگاه </label>
    <textarea wire:model="content"
              class="form-control @error('content') is-invalid @enderror"
              id="exampleFormControlTextarea1"
              rows="6"></textarea>
    @error('content') <span class=" text-danger">{{ $message }}</span> @enderror
   </div>
   <div class="row mb-3">
    <div class="col-md-6">
     <label for="inputEmail4"
            class="form-label">نام*</label>
     <input type="text"
            wire:model="name"
            class="form-control m-0 @error('name') is-invalid @enderror"
            id="inputEmail4">
     @error('name') <span class=" text-danger">{{ $message }}</span> @enderror
    </div>
    <div class="col-md-6">
     <label for="inputPassword4"
            class="form-label">ایمیل*</label>
     <input type="email"
            wire:model="email"
            class="form-control m-0 @error('email') is-invalid @enderror"
            id="inputPassword4">
     @error('email') <span class=" text-danger">{{ $message }}</span> @enderror
    </div>
   </div>
   <div class="col-12 mb-3">
    <label for="inputAddress"
           class="form-label">وب سایت</label>
    <input type="text"
           wire:model="site"
           class="form-control m-0 @error('site') is-invalid @enderror"
           id="inputAddress">
    @error('site') <span class=" text-danger">{{ $message }}</span> @enderror
   </div>
   <button class="btn btn-primary"
           wire:click.prevent="storeComment">ارسال دیدگاه
   </button>
  </form>
 </div>
</div>

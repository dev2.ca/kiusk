<div>
 <div class="section-title clearfix">
  <h2>اطلاعات آگهی </h2>
 </div>
 <form class="row g-3">
  <div class="col-md-8">
   <label for="inputEmail4"
          class="form-label">عنوان آگهی</label>
   <input type="text"
          wire:model="ad.title"
          class="form-control  @error('ad.title') is-invalid @enderror"
          id="inputEmail4">
   @error('ad.title') <span class=" text-danger">{{ $message }}</span> @enderror
  </div>
  <div class="col-md-4 price-2 position-relative">
   <label for="inputPassword4price"
          class="form-label">مبلغ</label>
   <input type="number"
          style="direction: rtl !important;"
          wire:model="ad.price"
          class="form-control  @error('ad.price') is-invalid @enderror"
          id="inputPassword4price">
   @error('ad.price') <span class=" text-danger">{{ $message }}</span> @enderror
  </div>
  <div class="col-md-6">
   <label for="formGroupExampleInput2"
          class="form-label">ایمیل</label>
   <input type="email"
          disabled
          value="{{auth()->user()->email}}"
          class="form-control"
          id="formGroupExampleInput2"
          placeholder="ایمیل">
  </div>
  <div class="col-md-6">
   <label for="formGroupExampleInput2"
          class="form-label">شماره تماس</label>
   <input type="text"
          disabled
          value="{{auth()->user()->phone}}"
          class="form-control "
          id="formGroupExampleInput2"
          placeholder="شماره تماس">
  </div>
  <div class="col-12">
   <div class="form-check">
    <input class="form-check-input"
           type="checkbox"
           wire:model="showEmail"
           id="gridCheck">
    <label class="form-check-label"
           for="gridCheck">
     ایمیل در آگهی نمایش داده نشود
    </label>
   </div>
  </div>
  <div class="form-floating">
                            <textarea class="form-control @error('ad.content') is-invalid @enderror"
                                      wire:model="content"
                                      placeholder="توضیحات آگهی"
                                      id="floatingTextarea2"
                                      style="height: 100px"></textarea>
   <label for="floatingTextarea2">توضیحات آگهی</label>
   @error('ad.content') <span class=" text-danger">{{ $message }}</span> @enderror
  </div>
  <div class="row g-3">
   <div class="col">
    <select class="form-select @error('ad.state_id') is-invalid @enderror"
            wire:model="ad.state_id"
            aria-label="Default select example">
     <option selected>استان</option>
     @foreach(\App\Models\Address\State::all() as $state )
      <option value="{{$state->id}}">{{$state->name}}</option>
     @endforeach
    </select>
    @error('ad.state_id') <span class=" text-danger">{{ $message }}</span> @enderror
   </div>
   <div class="col">
    <select class="form-select @error('ad.city_id') is-invalid @enderror"
            wire:model="ad.city_id"
            aria-label="Default select example">
     <option selected>شهر</option>
     @foreach(\App\Models\Address\City::whereStateId($ad->state_id)->get() as $state )
      <option value="{{$state->id}}">{{$state->name}}</option>
     @endforeach
    </select>
    @error('ad.city_id') <span class=" text-danger">{{ $message }}</span> @enderror
   </div>
   <div class="row g-3">
    @include('front.pages.ads.create.attributes',['formAttributes'=>$formAttributes])
   </div>
   <div class="section-title clearfix ">
    <h2>تصاویر آگهی</h2>
   </div>
   <p class="text-center">افزودنِ عکس بازدید آگهی شما را تا سه برابر افزایش می‌دهد.</p>
   <div class="container-file ">
    <div class="dropzone">
     <label for="files"
            class="dropzone-container">
      <div class="file-icon">+</div>
      <div class="dropzone-title">
       جهت بارگذاری تصویر کلیک کنید
      </div>
      <div class="spinner-border"
           role="status"
           wire:loading
           wire:target="photos">
       <span class="visually-hidden">Loading...</span>
      </div>
     </label>
     <div class="d-flex flex-wrap justify-content-around">
      @foreach($previewPhotos as $photo)
       <div class="position-relative mb-1">
        <img class="img-thumbnail "
             height="200"
             width="200"
             src="{{$photo->original_url}}">
        <span class="position-absolute right-0 start-100 translate-middle p-2   rounded-circle">
         <i class="fa fa-trash"
            aria-hidden="true"
            wire:click="mediaDelete({{$photo->id}})"></i>
         <span class="visually-hidden">New alerts</span>
        </span>
       </div>
      @endforeach
     </div>
     <input id="files"
            type="file"
            class="file-input"
            multiple
            wire:model="photos"/>
    </div>
   </div>
   @php
    $message='';
   @endphp
   @foreach($errors->getMessageBag()->messages() as $key=>$error)
    @if (Str::is('photos*',$key))
     @foreach ($error as $e)
      @php
       $message.=$e;
      @endphp
     @endforeach
    @endif
   @endforeach
   @if($message)
    <span class=" text-danger">{{ $message }}</span>
   @endif
  </div>
  <div class="alert alert-info"
       role="alert"><i class="fa fa-exclamation-triangle"></i>
   توجه: آگهی شما پس از بررسی و تأیید مدیریت در سایت منتشر خواهد شد.
  </div>
  <button class="btn btn-info col-3"
          wire:click.prevent="update">بروزرسانی آگهی
  </button>
 </form>
</div>

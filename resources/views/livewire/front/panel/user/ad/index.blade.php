<div>
 <div class="section-title clearfix p-3 pt-md-0">
  <h2>آگهی های من</h2>
  <a href="{{route('front.ad.create')}}"
     class="rounded-pill p-1 ps-2 pe-2 btn btn-ads position-absolute end-0 me-3 me-md-0">+ ثبت
   آگهی
  </a>
 </div>
 <div class="mt-5 mt-lg-0">
  @forelse(App\Models\Ad\Ad::whereUserId(auth()->id())->get() as $ad)
   <div class="d-flex align-items-end bg-white p-0 p-lg-2 rounded pt-lg-4 pb-lg-4 position-relative flex-wrap flex-lg-nowrap shadow-2 @if(!$loop->first)mt-4
@endif">
    <img src="{{$ad?->getFirstMedia('SpecialImage')?->getUrl('70_70')}}"
         alt=""
         width="100px"
         height="100px"
         class="rounded image-cart">
    <div class=" ms-2 p-1 pb-3 p-lg-0">
     <a href="{{route('front.ad.show',$ad->slug)}}">
      <h3>{{$ad->title}}</h3>
     </a>
     <div class="mt-3 little-font">
      <span class=" text-secondary"><i class="far fa-calendar-week m-1"></i>
       {{jdate($ad->created_at)->ago()}}</span>
      <span class="text-secondary ms-1"><i class="fas fa-folder-open m-1"></i>
       {{$ad?->mainCategory?->first()?->name}}</span>
      <span class="text-secondary ms-1"><i class="far fa-chart-bar m-1"></i>{{$ad->views}}
       بازدید</span>
      @if(!$ad->is_visible)
       <span class="btn-danger ms-1 p-2 rounded"> در انتظار بررسی</span>
      @else
       <span class="btn-success active ms-1 p-2 rounded">تایید شده</span>
      @endif
     </div>
    </div>
    <div class="card-list">
     <ul class="p-0">
      <li>
       <a class="text-black"
          href="{{route('front.panel.user.ad.edit',$ad->id)}}"><i class="fas fa-pencil-alt"></i>ویرایش آگهی</a>
      </li>
      <li>
       <a class="text-black"
          href="{{route('front.panel.user.ad.payment',$ad->id)}}">
        <i class="fal fa-sort-amount-up-alt"></i>ارتقا آگهی
       </a>
      </li>
      <li wire:click.prevent="beforeDelete({{$ad->id}})">
       <i class="far fa-times"></i>حذف آگهی
      </li>
     </ul>
    </div>
   </div>
  @empty
   <div>
    <p>آگهی تاکنون ثبت نکرده اید.</p>
   </div>
  @endforelse
 </div>
</div>
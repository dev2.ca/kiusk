<div class="section-title clearfix">
 <h2>پرداخت های من</h2>
</div>
<div class="">
 <div class="container table-responsive  p-0 p-lg-4 pt-lg-0">
  <table class="table table-bordered">
   <thead class="">
   <tr>
    <th scope="col">آیدی پرداخت</th>
    <th scope="col">تاریخ</th>
    <th scope="col">وضعیت</th>
    <th scope="col">مجموع</th>
    <th scope="col">توضیحات</th>
    {{--    <th scope="col">پرداخت</th>--}}
   </tr>
   </thead>
   <?php
   use Akaunting\Money\Money;
   ?>
   <tbody>
   @foreach(\App\Models\Payment\Payment::all() as $payment)
    <tr class="bg-white">
     <td>
      #{{$payment->id}}
     </td>
     <td>
      {{jdate($payment->created_at)->format('Y F d')}}
     </td>
     <td>
      @switch($payment->status)
       @case('complete')
       پرداخت شده
       @break
      @endswitch
     </td>
     <td>
      {{--         {{$price}}--}}
      {!! Money::USD($payment->amount, true)  !!}
     </td>
     <td>
      @if($payment->payable instanceof App\Models\Ad\Ad)

       @switch($payment?->extra?->ad?->name)
        @case('آگهی ویژه')<i class="fas fa-star"></i>@break
        @case('نردبان')<i class="fas fa-rocket"></i>@break
        @case('آگهی ویژه و نردبان')
        <i class="fas fa-star"></i>
        <i class="fas fa-rocket"></i>@break
       @endswitch
       {{$payment?->extra?->ad?->name??''}}

       <br>


       <a href="{{route('front.ad.show',$payment->payable->slug)}}">{{$payment->payable->title}}</a>
       {{--       در تاریخ--}}
       {{--       {{jdate($payment->created_at)}}--}}
      @endif
     </td>
     {{--    <td><button class="btn btn-success">پرداخت</button></td>--}}
    </tr>
   @endforeach

   {{--   <tr class="bg-white">--}}

   {{--    <td colspan="4">--}}
   {{--     <form action="" class="d-flex align-items-center justify-content-end">--}}
   {{--      <label for="" class="me-3">کد تخفیف</label>--}}
   {{--      <input type="text" class="form-control m-0">--}}
   {{--      <button class="btn btn-primary m-0">اعمال</button>--}}
   {{--     </form>--}}
   {{--    </td>--}}
   {{--   </tr>--}}
   </tbody>
  </table>
 </div>
</div>

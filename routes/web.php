<?php

use App\Http\Controllers\Admin\CkeditorController;
use App\Http\Controllers\Front\Ad\AdsController;
use App\Http\Controllers\Front\Blog\BlogController;
use App\Http\Controllers\Front\Home\HomeController;
use App\Http\Controllers\Front\Panel\User\UserPanelController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\Payment\PaymentController;
use App\Http\Controllers\SeedController;
use App\Http\Controllers\SeedPostController;
use App\Http\Controllers\TelegramController;
use App\Models\Ad\Ad;
use App\Models\Ad\Category;
use App\Models\Blog\Post;
use App\Models\User;

//use Corcel\Model\Post;
use Corcel\Model\Taxonomy;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Telegram\Bot\Api;
use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * @param $post
 * @return Closure
 */
//seed
Route::group(['as' => ''], function () {
    Route::get('seed/all', function () {
        ini_set('max_execution_time', 0);
        (new SeedController())->adAll();
        (new SeedPostController())->postAll();
        return 'seed all successful';
    });
    Route::get('seed/ad/all', [
        SeedController::class,
        'adAll',
    ]);
    Route::get('seed/ads', [
        SeedController::class,
        'ads',
    ]);
    Route::get('seed/tags', [
        SeedController::class,
        'tags',
    ]);
    Route::get('seed/categories', [
        SeedController::class,
        'categories',
    ]);
    Route::get('seed/post/all', [
        SeedPostController::class,
        'postAll',
    ]);
    Route::get('seed/posts', [
        SeedPostController::class,
        'posts',
    ]);
    Route::get('seed/posts/tags', [
        SeedPostController::class,
        'tags',
    ]);
    Route::get('seed/posts/categories', [
        SeedPostController::class,
        'categories',
    ]);
});
//old Front
/*Route::group(['prefix' => ''], function () {
 //home
 Route::view('/', 'base')
      ->name('home');
 Route::get('product-category/{slug}/page/{page?}', [
  PageController::class,
  'adCategory'
 ])

  //ads
      ->name('ads-category');
 Route::get('product-category/{slug}', [
  PageController::class,
  'adCategory'
 ])
      ->name('index-ads-category');
 Route::get('product-tag/{slug}/page/{page?}', [
  PageController::class,
  'adTag'
 ])
      ->name('ads-tag');
 Route::get('product-tag/{slug}', [
  PageController::class,
  'adTag'
 ])
      ->name('index-ads-tag');
 Route::get('ads/{slug}', [
  PageController::class,
  'ad'
 ])
      ->name('ad');
//blog
 Route::get('weblog-2/{page?}', [
  BlogController::class,
  'index'
 ])
      ->name('blog-index');
 Route::get('blog/{year}/{month}/{day}/{slug}', [
  BlogController::class,
  'post'
 ])
      ->name('blog-post');
 Route::get('tags/{slug}', [
  BlogController::class,
  'tags'
 ])
      ->name('post-tags');
});*/
// Admin
Route::group([
    'as' => 'admin.',
    'middleware' => 'throttle'
], function () {
    Route::post('ckeditor/upload', [
        CkeditorController::class,
        'upload'
    ])
        ->name('ckeditor.upload');
});
//new Front
Route::group([
    'as' => 'front.',
    'middleware' => ['cacheResponse']
], function () {
    Route::get('', [
        HomeController::class,
        'frontHome'
    ])
        ->middleware('cacheResponse')
        ->name('home');
    Route::get('login-register', [
        HomeController::class,
        'frontLoginRegister'
    ])
        ->withoutMiddleware('cacheResponse')
        ->name('login-register');
    Route::get('قوانین-و-مقررات', [
        HomeController::class,
        'frontRules'
    ])
        ->name('rules');
    Route::get('contact', [
        HomeController::class,
        'frontContactUs'
    ])
        ->name('contact-us');
    Route::get('about', [
        HomeController::class,
        'frontAboutUs'
    ])
        ->name('about-us');
    Route::group(['as' => 'ad.'], function () {
//  Route::get('', [
//   AdsController::class,
//   'frontAdSearch'
//  ])
//       ->name('search');
        Route::get('listing', [
            AdsController::class,
            'frontAdIndex'
        ])
            ->name('index.first.page');
        Route::get('listing/page/{page?}', [
            AdsController::class,
            'frontAdIndex'
        ])
            ->name('index');
        Route::get('newad', [
            AdsController::class,
            'frontAdCreate'
        ])
            ->name('create')
            ->middleware('auth');
        Route::get('ads/{slug:slug}', [
            AdsController::class,
            'frontAdShow'
        ])
            ->name('show');
        Route::group(['as' => 'category.city.'], function () {
            Route::get('blog/city_categories/{slug}/page/{page?}', [
                AdsController::class,
                'frontAdCategoryCityIndex'
            ])
                ->name('index');
            Route::get('blog/city_categories/{slug}', [
                AdsController::class,
                'frontAdCategoryCityIndex'
            ])
                ->name('index.first.page');
        });
        Route::group(['as' => 'category.'], function () {
            Route::get('product-category/{slug}/page/{page?}', [
                AdsController::class,
                'frontAdCategoryIndex'
            ])
                ->name('index');
            Route::get('product-category/{slug}', [
                AdsController::class,
                'frontAdCategoryIndex'
            ])
                ->name('index.first.page');
        });
        Route::group(['as' => 'tag.'], function () {
            Route::redirect('product-tag/{slug}/page/{page?}', config('app.url')
//    , [
//    AdsController::class,
//    'frontAdTagIndex'
//   ]
            );
//        ->name('index');
            Route::redirect('product-tag/{slug}', config('app.url')
//    , [
//    AdsController::class,
//    'frontAdTagIndex'
//   ]
            );
//        ->name('index.first.page');
        });
    });
    Route::group(['as' => 'blog.'], function () {
        Route::get('blog/{year}/{month}/{day}/{slug}', [
            BlogController::class,
            'frontBlogShow'
        ])
            ->name('show');
        Route::group(['as' => 'category.'], function () {
            Route::get('weblog-2/{page?}', [
                BlogController::class,
                'frontBlogCategoryIndexBlog'
            ])
                ->name('blog.index');
            Route::get('weblog-2', [
                BlogController::class,
                'frontBlogCategoryIndexBlog'
            ])
                ->name('blog.index.first.page');
            Route::get('اخبار/{page}', [
                BlogController::class,
                'frontBlogCategoryIndexNews'
            ])
                ->name('news.index');
            Route::get('اخبار', [
                BlogController::class,
                'frontBlogCategoryIndexNews'
            ])
                ->name('news.index.first.page');
        });
        Route::group(['as' => 'tag.'], function () {
            Route::redirect('tags/{slug}/page/{page}', config('app.url')
//    , [
//    BlogController::class,
//    'frontBlogTagIndex'
//   ]
            );
//        ->name('index');
            Route::redirect('tags/{slug}', config('app.url')
//              [
//    BlogController::class,
//    'frontBlogTagIndex'
//   ]
            );
//        ->name('index.first.page');
        });
    });
    Route::group([
        'as' => 'panel.user.',
        'middleware' => 'auth'
    ], function () {
        Route::get('profile/{user?}', [
//  Route::get('profile/?user={user?}', [
            UserPanelController::class,
            'frontPanelUserProfileShow'
        ])
            ->name('profile.show');
        Route::group([
            'prefix' => 'my-account'
        ], function () {
            Route::get('', [
                UserPanelController::class,
                'frontPanelUserAdIndex'
            ])
                ->name('ad.index');
            Route::get('edit-ad/{id}', [
                UserPanelController::class,
                'frontPanelUserAdEdit'
            ])
                ->name('ad.edit');
            Route::get('edit-ad/{id}/payment', [
                UserPanelController::class,
                'frontPanelUserAdPayment'
            ])
                ->name('ad.payment');
            Route::get('user-bookmark', [
                UserPanelController::class,
                'frontPanelUserFavoriteIndex'
            ])
                ->name('favorite.index')
                ->withoutMiddleware('auth');
            Route::get('orders', [
                UserPanelController::class,
                'frontPanelUserPaymentIndex'
            ])
                ->name('payment.index');
            Route::get('edit-account', [
                UserPanelController::class,
                'frontPanelUserProfileEdit'
            ])
                ->name('profile.edit');
        });
    });
});
//telegram
Route::group(['as' => ''], function () {
    /**
     * todo un comment
     */
// Route::post(st()->botApiToken, [
    Route::post('5000545191:AAEQRciGRoWXjw42zwHyqJMUWaQB53WFezw', [
        TelegramController::class,
        'index'
    ]);
    Route::get('/setwebhook', function () {
        return $response = Telegram::setWebhook(['url' => 'https://admin.razzar.ir/5000545191:AAEQRciGRoWXjw42zwHyqJMUWaQB53WFezw']);
    });
    Route::get('/deletewebhook', function () {
        $t = new Api('5000545191:AAEQRciGRoWXjw42zwHyqJMUWaQB53WFezw');
        return $t->deleteWebhook();
    });
    Route::get('/resetwebhook', function () {
        $t = new Api('5000545191:AAEQRciGRoWXjw42zwHyqJMUWaQB53WFezw');
        $t->deleteWebhook();
        $all = $t->getUpdates(['offset' => 423501055]);
        $response = Telegram::getWebhookInfo();
        $response2 = Telegram::setWebhook(['url' => 'https://admin.razzar.ir/5000545191:AAEQRciGRoWXjw42zwHyqJMUWaQB53WFezw']);
        return [
            $t,
            $all,
            $response,
            $response2,
        ];
    });
    Route::get('/getwebhook', function () {
        return $response = Telegram::getWebhookInfo();
    });
    Route::get('/getUpdates', function () {
        $t = new Api('5000545191:AAEQRciGRoWXjw42zwHyqJMUWaQB53WFezw');
        return $t->getUpdates(['offset' => 423501055]);
    });
    Route::get('/t', function () {
        $r = new App\Http\Controllers\TelegramController();
        $r->adsList();
    });
});
//payment form
//Route::get('/pay', [
// PaymentController::class,
// 'index'
//]);
//// route for processing payment
//Route::post('paypal', [
// PaymentController::class,
// 'payWithpaypal'
//]);
// route for check status of the payment
Route::get('status', [
    PaymentController::class,
    'getPaymentStatus'
])
    ->name('payment');
Route::get('/test', function () {
    return
//        DB::table('telescope_entries')->count();
//    $this->table('telescope_monitoring')->delete();
//    return now()->toDateTimeString();
//return
//    Laravel\Telescope\Storage\EntryModel::oldest()->first()->created_at->ago();
//
//    return
//    User::whereEmail('farhad.3.rohani@gmail.com')->update([
//        'password' => bcrypt('farhad.3.rohani@gmail.com'),
//        'rule'=>'admin'
//]);

// a=\Spatie\LaravelSettings\Models\SettingsProperty::create([
//        'group' => 'general',
//        'name' => 'allowViewTelescopeUsers',
//        'locked' => '0',
//        'payload' => json_encode([
//            [
//                'email'=>'farhad.3.rohani@gmial.com',
//                'name'=>'farhad rohani moghaddas'
//            ]
//        ])
//    ]);
    \Spatie\LaravelSettings\Models\SettingsProperty::create([
        'group' => 'general',
        'name' => 'telescopeRecordAll',
        'locked' => '0',
        'payload' => 'false'
    ]);
//    \Spatie\LaravelSettings\Models\SettingsProperty::find(67)->update([
//        'payload' => "\"[{\\\"email\\\":\\\"farhad.3.rohani@gmail.com\\\",\\\"name\\\":\\\"farhadrohanimoghaddas\\\"}]\""
//    ]);
//    return
//    \Spatie\LaravelSettings\Models\SettingsProperty::whereIn('name', ['telescopeNightMode', 'allowViewTelescopeUsers'])->get();
});
Route::get('/artisan', function () {
    Artisan::call("optimize:clear");
// Artisan::call("view:clear");
});
